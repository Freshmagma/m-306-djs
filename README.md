# GIBZ App - Mobile Client

This repository contains the codebase for the mobile application intended to run on Android and iOS.

To run the application in development mode, it is recommended to follow the corresponding chapters of Flutters [Get started guid](https://docs.flutter.dev/get-started/install):

- [Install](https://docs.flutter.dev/get-started/install)
- [Setup an editor](https://docs.flutter.dev/get-started/editor)
- [Run the app (omit paragraph *Create the app*)](https://docs.flutter.dev/get-started/test-drive)

Depending on the scope of functionality you plan to develop, it might be sufficient to run the app in a regular web browser like *Chrome*. This way, you can save yourself the time-consuming installation for mobile platforms (such as Android Studio and/or Xcode).