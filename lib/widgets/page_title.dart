import 'package:flutter/material.dart';

class PageTitle extends StatelessWidget {
  const PageTitle({
    required this.title,
    this.textColor,
    super.key,
  });

  final String title;
  final Color? textColor;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 15,
        bottom: 10,
      ),
      child: Text(
        title,
        style: textColor == null
            ? Theme.of(context).textTheme.headlineLarge
            : Theme.of(context)
                .textTheme
                .headlineLarge!
                .copyWith(color: textColor),
      ),
    );
  }
}
