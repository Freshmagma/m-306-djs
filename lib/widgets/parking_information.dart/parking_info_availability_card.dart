import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/data/repositories/parking_information/car_park_repository.dart';
import 'package:gibz_mobileapp/widgets/parking_information.dart/availability_indicator_bar.dart';

class ParkingInfoAvailabilityCard extends StatelessWidget {
  const ParkingInfoAvailabilityCard({super.key});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: context.read<CarParkRepository>().getCurrentAvailability(),
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        if (!snapshot.hasData || snapshot.hasError) {
          return Center(
            child: Text(
              'Aktuell sind keine Informationen zum Parkhaus am GIBZ verfügbar.',
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodySmall!.copyWith(
                    color:
                        Theme.of(context).colorScheme.tertiary.withAlpha(100),
                  ),
            ),
          );
        }

        final availability = snapshot.data!.currentAvailability;
        return Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
          padding: const EdgeInsets.all(15),
          child: (snapshot.connectionState != ConnectionState.done)
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        availability.status,
                        style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: availability.isOpen
                                  ? const Color.fromRGBO(0, 161, 96, 1)
                                  : const Color.fromRGBO(238, 58, 65, 1),
                              height: 1,
                            ),
                      ),
                    ),
                    Text(
                      '${availability.free}',
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                            fontSize: 48,
                            fontWeight: FontWeight.w800,
                            height: 1,
                          ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 7,
                        bottom: 10,
                      ),
                      child: Text(
                        'Freie\nParkplätze',
                        style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                              fontSize: 32,
                              fontWeight: FontWeight.w600,
                              height: 1.3,
                            ),
                      ),
                    ),
                    AvailabilityIndicatorBar(
                      currentAvailability: availability.free,
                      maximumAvailability: 82,
                    ),
                  ],
                ),
        );
      },
    );
  }
}
