import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/data/api/profile_picture/profile_picture_api.dart';
import 'package:gibz_mobileapp/data/api/rally/participating_party_api.dart';
import 'package:gibz_mobileapp/data/api/rally/rally_stage_api.dart';
import 'package:gibz_mobileapp/data/api/rally/stage_activity_result_api.dart';
import 'package:gibz_mobileapp/data/dio.dart';
import 'package:gibz_mobileapp/data/repositories/meal_information/meal_repository.dart';
import 'package:gibz_mobileapp/data/repositories/parking_information/car_park_repository.dart';
import 'package:gibz_mobileapp/data/repositories/students_manual/article_repository.dart';
import 'package:gibz_mobileapp/services/rally/beacon_service.dart';
import 'package:gibz_mobileapp/state_management/progress_indicator_cubit.dart';

import 'gibz_bloc_provider.dart';

class GibzRepositoryProvider extends StatelessWidget {
  final Widget child;
  const GibzRepositoryProvider({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => ProgressIndicatorCubit(),
      child: MultiRepositoryProvider(
        providers: [
          RepositoryProvider<ArticleRepository>(
            create: (_) => ArticleRepository(),
          ),
          RepositoryProvider<ParticipatingPartyApi>(
            create: (context) => ParticipatingPartyApi(
              DioClient(),
              context.read<ProgressIndicatorCubit>(),
            ),
          ),
          RepositoryProvider<RallyStageApi>(
            create: (context) => RallyStageApi(DioClient()),
          ),
          RepositoryProvider<StageActivityResultApi>(
            create: (context) => StageActivityResultApi(DioClient()),
          ),
          RepositoryProvider(create: (_) => ProfilePictureApi(DioClient())),
          RepositoryProvider<MealRepository>(
            create: (context) => MealRepository(),
          ),
          RepositoryProvider(
            create: ((context) => CarParkRepository()),
          ),
          // Todo: Since BeaconService is not a repository - use actual dependency injection
          RepositoryProvider<BeaconService>(
            create: (_) => BeaconService(),
          )
        ],
        child: GibzBlocProvider(child: child),
      ),
    );
  }
}
