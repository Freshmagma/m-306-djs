import 'package:flutter/material.dart';

class NoContentHint extends StatelessWidget {
  const NoContentHint({super.key, required this.message});

  final String message;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 30,
        vertical: 50,
      ),
      child: Text(
        message,
        style: DefaultTextStyle.of(context).style.copyWith(
              fontSize: 18,
              color: Colors.grey,
            ),
        textAlign: TextAlign.center,
      ),
    );
  }
}
