import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:gibz_mobileapp/utilities/default_markdown_stylesheet.dart';
import 'package:url_launcher/url_launcher.dart';

class MarkdownText extends StatelessWidget {
  const MarkdownText({required this.data, super.key});

  final String data;

  @override
  Widget build(BuildContext context) {
    return MarkdownBody(
      data: data,
      styleSheet: defaultMarkdownStylesheet,
      onTapLink: (text, url, title) {
        if (url != null && url.isNotEmpty) {
          launchUrl(Uri.parse(url));
        }
      },
    );
  }
}
