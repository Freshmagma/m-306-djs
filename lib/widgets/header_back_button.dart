import 'package:flutter/material.dart';

class HeaderBackButton extends StatelessWidget {
  const HeaderBackButton({
    required this.onTap,
    this.color = Colors.black,
    super.key,
  });

  final void Function()? onTap;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: onTap,
      child: Row(
        children: [
          Icon(
            Icons.arrow_back,
            color: color,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8),
            child: Text(
              'zurück',
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium!
                  .copyWith(color: color),
            ),
          ),
        ],
      ),
    );
  }
}
