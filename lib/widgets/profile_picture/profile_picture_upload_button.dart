import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/profile_picture/cubit/profile_picture_approval_cubit.dart';
import 'package:gibz_mobileapp/state_management/profile_picture/profile_picture_bloc.dart';
import 'package:gibz_mobileapp/state_management/progress_indicator_cubit.dart';

class ProfilePictureUploadButton extends StatelessWidget {
  const ProfilePictureUploadButton({
    super.key,
    required this.token,
    required this.file,
  });

  final String token;
  final XFile file;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfilePictureApprovalCubit,
        Map<ProfilePictureApprovalCriteria, bool>>(
      builder: (context, approvalState) {
        return BlocBuilder<ProgressIndicatorCubit, LoadingState>(
          builder: (context, loadingState) {
            return ElevatedButton(
              onPressed: !loadingState
                          .contains(LoadingAspect.uploadingProfilePicture) &&
                      approvalState.values.every((value) => value)
                  ? () {
                      context
                          .read<ProgressIndicatorCubit>()
                          .start(LoadingAspect.uploadingProfilePicture);
                      context.read<ProfilePictureBloc>().add(PictureApproved(
                            token: token,
                            file: file,
                          ));
                    }
                  : null,
              child:
                  loadingState.contains(LoadingAspect.uploadingProfilePicture)
                      ? const SizedBox(
                          width: 15,
                          height: 15,
                          child: CircularProgressIndicator(
                            strokeWidth: 2,
                          ),
                        )
                      : const Text('Foto bestätigen'),
            );
          },
        );
      },
    );
  }
}
