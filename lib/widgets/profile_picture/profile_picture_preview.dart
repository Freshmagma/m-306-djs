import 'dart:io';

import 'package:flutter/material.dart';

class ProfilePicturePreview extends StatelessWidget {
  const ProfilePicturePreview({
    super.key,
    required this.path,
  });

  final String path;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      child: AspectRatio(
        aspectRatio: 3 / 4,
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                blurRadius: 10,
                spreadRadius: 2,
                color: Color.fromRGBO(82, 82, 82, 0.2),
              )
            ],
          ),
          padding: const EdgeInsets.all(4),
          child: Image.file(
            File(path),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
