import 'package:flutter/material.dart';

class FaceMask extends StatelessWidget {
  const FaceMask({
    super.key,
    required this.height,
  });

  final double height;

  @override
  Widget build(BuildContext context) {
    return ColorFiltered(
      colorFilter: ColorFilter.mode(
        Colors.black.withOpacity(0.42),
        BlendMode.srcOut,
      ),
      child: Stack(
        fit: StackFit.expand,
        children: [
          Container(
            decoration: const BoxDecoration(
              color: Colors.black,
              backgroundBlendMode: BlendMode.dstOut,
            ),
          ),
          Align(
            child: Container(
              margin:
                  EdgeInsets.only(bottom: (height * 0.5) - (height * (1 / 4))),
              width: 200,
              height: 300,
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(200),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
