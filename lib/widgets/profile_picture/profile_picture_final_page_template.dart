import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';

class ProfilePictureFinalPageTemplate extends StatelessWidget {
  const ProfilePictureFinalPageTemplate({
    required this.color,
    required this.iconName,
    required this.children,
    super.key,
  });

  final Color color;
  final String iconName;
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: color,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(30),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SvgPicture.asset('assets/icons/profile_picture/$iconName'),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [...children],
              ),
              ElevatedButton(
                onPressed: () {
                  context.go('/');
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateColor.resolveWith((states) => Colors.white),
                  foregroundColor: MaterialStateColor.resolveWith(
                    (states) => color,
                  ),
                ),
                child: const Text('Beenden'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
