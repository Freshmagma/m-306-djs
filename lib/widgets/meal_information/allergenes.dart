import 'package:flutter/material.dart';

class Allergenes extends StatelessWidget {
  const Allergenes({required this.allergenes, super.key});

  final List<String> allergenes;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 15),
          child: Text('Allergene',
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium!
                  .copyWith(fontWeight: FontWeight.w600)),
        ),
        Text(
          allergenes.join(', '),
          style: Theme.of(context).textTheme.bodyMedium,
        ),
      ],
    );
  }
}
