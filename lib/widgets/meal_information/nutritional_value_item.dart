import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/models/meal_information/nutritional_value.dart';

class NutritionalValueItem extends StatelessWidget {
  const NutritionalValueItem({
    required this.iconName,
    required this.value,
    required this.label,
    super.key,
  });

  factory NutritionalValueItem.forItem(NutritionalValue item) {
    switch (item.title) {
      case 'Energie':
        return NutritionalValueItem.energy(item.value);
      case 'Eiweiss':
        return NutritionalValueItem.protein(item.value);
      case 'Fett':
        return NutritionalValueItem.fat(item.value);
      case 'Kohlenhydrate':
        return NutritionalValueItem.carbs(item.value);
      case 'Salz':
        return NutritionalValueItem.salt(item.value);
      default:
        return NutritionalValueItem(
          iconName: 'energy',
          value: item.value,
          label: item.title,
        );
    }
  }

  // ignore: use_key_in_widget_constructors
  const NutritionalValueItem.energy(String value)
      : this(iconName: 'energy', label: 'Energie', value: value);

  // ignore: use_key_in_widget_constructors
  const NutritionalValueItem.carbs(String value)
      : this(iconName: 'carbs', label: 'Kohlenhydrate', value: value);

  // ignore: use_key_in_widget_constructors
  const NutritionalValueItem.protein(String value)
      : this(iconName: 'protein', label: 'Protein', value: value);

  // ignore: use_key_in_widget_constructors
  const NutritionalValueItem.fat(String value)
      : this(iconName: 'fats', label: 'Fett', value: value);

  // ignore: use_key_in_widget_constructors
  const NutritionalValueItem.salt(String value)
      : this(iconName: 'salt', label: 'Salz', value: value);

  final String iconName;
  final String value;
  final String label;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 3, 10, 3),
          child:
              SvgPicture.asset('assets/icons/meal_information/$iconName.svg'),
        ),
        Text(
          '$value $label',
          style: Theme.of(context).textTheme.bodyMedium,
        ),
      ],
    );
  }
}
