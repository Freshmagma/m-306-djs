import 'package:flutter/material.dart';
import 'package:gibz_mobileapp/models/meal_information/meal.dart';
import 'package:gibz_mobileapp/widgets/meal_information/allergenes.dart';
import 'package:gibz_mobileapp/widgets/meal_information/meal_indicator_pill.dart';
import 'package:gibz_mobileapp/widgets/meal_information/meal_title.dart';
import 'package:gibz_mobileapp/widgets/meal_information/nutritional_value_item.dart';

class MealDetailData extends StatelessWidget {
  const MealDetailData({required this.meal, super.key});

  final Meal meal;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 20,
            horizontal: 30,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MealTitle(
                title: meal.menuComponents[0],
                subtitle: meal.title,
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15),
                child: Text(
                  meal.menuComponents.skip(1).join('\n'),
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
              ),
              ...(meal.nutritionalValues
                    ..sort((a, b) => a.rowIndex.compareTo(b.rowIndex)))
                  .map((item) => NutritionalValueItem.forItem(item)),
              if (meal.allergenes.isNotEmpty)
                Allergenes(allergenes: meal.allergenes),
              if (meal.balance != null && meal.balance!.isNotEmpty)
                MealIndicatorPill.balance(meal.balance!),
              if (meal.environmentalImpact != null &&
                  meal.environmentalImpact!.isNotEmpty)
                MealIndicatorPill.environmentalImpact(
                    meal.environmentalImpact!),
              Container(
                height: 90,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
