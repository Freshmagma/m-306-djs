import 'package:flutter/material.dart';
import 'package:gibz_mobileapp/models/meal_information.dart';
import 'package:gibz_mobileapp/widgets/meal_information/meal_title.dart';
import 'package:go_router/go_router.dart';

class MenuDayMealListItem extends StatelessWidget {
  const MenuDayMealListItem({
    required this.meal,
    this.isLastItem = false,
    super.key,
  });

  final bool isLastItem;
  final Meal meal;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.push('/meal-information/${meal.id}');
      },
      child: Container(
        decoration: !isLastItem
            ? const BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    width: 2,
                    color: Color.fromRGBO(207, 212, 222, 1),
                  ),
                ),
              )
            : null,
        padding: const EdgeInsets.symmetric(vertical: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            MealTitle(
              title: meal.menuComponents[0],
              subtitle: meal.title,
            ),
            Text(
              meal.menuComponents.skip(1).join('\n'),
              style: Theme.of(context).textTheme.bodyMedium,
            ),
            if (meal.priceInternal > 0 || meal.priceExternal > 0)
              Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.primary,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  padding: const EdgeInsets.symmetric(
                    vertical: 3,
                    horizontal: 12,
                  ),
                  child: Text(
                    'ab ${meal.bestPrice.toStringAsFixed(2)} CHF',
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
