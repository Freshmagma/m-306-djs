import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MealIndicatorPill extends StatelessWidget {
  const MealIndicatorPill({
    required this.title,
    required this.label,
    required this.iconName,
    super.key,
  });

  // ignore: use_key_in_widget_constructors
  const MealIndicatorPill.balance(String label)
      : this(label: label, iconName: 'balance', title: 'Ausgewogenheit');

  // ignore: use_key_in_widget_constructors
  const MealIndicatorPill.environmentalImpact(String label)
      : this(label: label, iconName: 'impact', title: 'Umweltbelastung');

  final String title;
  final String label;
  final String iconName;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 15),
          child: Text(title,
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium!
                  .copyWith(fontWeight: FontWeight.w600)),
        ),
        Container(
          decoration: BoxDecoration(
            color: _getColorForLabel(label),
            borderRadius: BorderRadius.circular(20),
          ),
          margin: const EdgeInsets.only(top: 5),
          padding: const EdgeInsets.symmetric(
            vertical: 3,
            horizontal: 15,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: SvgPicture.asset(
                    'assets/icons/meal_information/$iconName.svg'),
              ),
              Text(
                label,
                style: Theme.of(context)
                    .textTheme
                    .bodyMedium!
                    .copyWith(color: Colors.white),
              )
            ],
          ),
        ),
      ],
    );
  }

  Color _getColorForLabel(String label) {
    switch (label.toLowerCase()) {
      case 'gering':
      case 'ausgewogen':
        return const Color.fromRGBO(0, 162, 91, 1);
      case 'mittel':
      case 'akzeptabel':
        return const Color.fromRGBO(255, 224, 78, 1);
      case 'hoch':
        return const Color.fromRGBO(246, 142, 30, 1);
      case 'sehr hoch':
      case 'unausgewogen':
        return const Color.fromRGBO(238, 58, 65, 1);
      default:
        return const Color.fromARGB(255, 182, 184, 186);
    }
  }
}
