import 'dart:math';

import 'package:flutter/material.dart';

class PricePill extends StatelessWidget {
  const PricePill({
    required this.internalPrice,
    required this.externalPrice,
    this.lowerPriceOnly = false,
    super.key,
  });

  final double internalPrice;
  final double externalPrice;
  final bool lowerPriceOnly;

  String get internalString => internalPrice.toStringAsFixed(2);
  String get externalString => externalPrice.toStringAsFixed(2);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      padding: const EdgeInsets.symmetric(
        vertical: 4,
        horizontal: 18,
      ),
      child: Text(
        _getPriceTag(),
        style: Theme.of(context).textTheme.labelLarge!.copyWith(
              fontWeight: FontWeight.bold,
              color: Theme.of(context).colorScheme.primary,
            ),
      ),
    );
  }

  String _getPriceTag() {
    if (lowerPriceOnly) {
      double lowerPrice = min(internalPrice, externalPrice);
      return 'ab ${lowerPrice.toStringAsFixed(2)} CHF';
    }

    return '$internalString / $externalString CHF';
  }
}
