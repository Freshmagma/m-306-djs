import 'package:flutter/material.dart';

class RallyCard extends StatelessWidget {
  const RallyCard({required this.child, super.key});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        boxShadow: const [
          BoxShadow(
            offset: Offset(0, 4),
            blurRadius: 10,
            spreadRadius: 2,
            color: Color.fromRGBO(82, 82, 82, 0.07),
          )
        ],
      ),
      child: child,
    );
  }
}
