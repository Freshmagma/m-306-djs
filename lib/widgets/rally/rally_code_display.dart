import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../state_management/rally/cubit/rally_code_cubit.dart';

class RallyCodeDisplay extends StatelessWidget {
  const RallyCodeDisplay({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RallyCodeCubit, String>(
      builder: (BuildContext context, String state) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            for (int i = 0; i <= 5; i++)
              RallyCodeDigit(
                digit: state.length > i ? state[i] : '',
                isActive: state.length == i + 1,
              )
          ],
        );
      },
    );
  }
}

class RallyCodeDigit extends StatelessWidget {
  const RallyCodeDigit({
    super.key,
    required this.digit,
    this.isActive = false,
  });

  final String digit;
  final bool isActive;

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Padding(
        padding: const EdgeInsets.all(5),
        child: AspectRatio(
          aspectRatio: 1,
          child: Container(
            decoration: BoxDecoration(
              color: isActive
                  ? Colors.white
                  : Theme.of(context).colorScheme.secondary,
              borderRadius: BorderRadius.circular(50),
            ),
            child: Center(
              child: Text(
                digit,
                style: DefaultTextStyle.of(context).style.copyWith(
                      color: isActive
                          ? Theme.of(context).colorScheme.primary
                          : Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
