import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/state_management/rally/cubit/stage_video_cubit.dart';
import 'package:gibz_mobileapp/state_management/rally/rally_bloc.dart';
import 'package:video_player/video_player.dart';

class CustomVideoPlayerControls extends StatelessWidget {
  const CustomVideoPlayerControls({
    required this.videoPlayerController,
    super.key,
  });

  final VideoPlayerController videoPlayerController;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: BlocProvider<StageVideoCubit>(
        create: (context) => StageVideoCubit(),
        child: LayoutBuilder(
          builder: (context, constraints) {
            return BlocBuilder<StageVideoCubit, StageVideoState>(
              builder: (context, state) {
                return BlocListener<RallyBloc, RallyState>(
                  listenWhen: (previous, current) =>
                      current is PendingStageActivity,
                  listener: (context, state) {
                    if (videoPlayerController.value.isPlaying) {
                      videoPlayerController.pause();
                      context.read<StageVideoCubit>().togglePlay();
                    }
                  },
                  child: Stack(
                    children: [
                      GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () {
                          context.read<StageVideoCubit>().toggleControls();
                        },
                        child: SizedBox(
                            width: constraints.maxWidth,
                            height: constraints.maxHeight,
                            child: Container(
                              color: state.showControls
                                  ? const Color.fromRGBO(0, 0, 0, 0.25)
                                  : Colors.transparent,
                            )),
                      ),
                      if (state.showControls)
                        GestureDetector(
                          onTap: () {
                            context.read<StageVideoCubit>().togglePlay();
                            if (videoPlayerController.value.isPlaying) {
                              videoPlayerController.pause();
                            } else {
                              videoPlayerController.play();
                            }
                          },
                          child: Center(
                            child: Container(
                              decoration: const BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: Color.fromRGBO(178, 178, 178, .25),
                                    blurRadius: 16,
                                  )
                                ],
                              ),
                              child: SvgPicture.asset(
                                videoPlayerController.value.isPlaying
                                    ? 'assets/icons/rally/pause.svg'
                                    : 'assets/icons/rally/play.svg',
                                width: 50,
                              ),
                            ),
                          ),
                        ),
                      if (state.showControls)
                        Positioned(
                          bottom: 10,
                          right: 10,
                          child: GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () {
                              ChewieController.of(context).toggleFullScreen();
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: const Color.fromRGBO(255, 255, 255, 0.7),
                                borderRadius: BorderRadius.circular(20),
                              ),
                              width: 35,
                              height: 35,
                              child: Center(
                                child: Icon(
                                  ChewieController.of(context).isFullScreen
                                      ? Icons.fullscreen_exit_rounded
                                      : Icons.fullscreen_rounded,
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                              ),
                            ),
                          ),
                        )
                    ],
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
