import 'package:flutter/material.dart';

class ScoreCardItem extends StatelessWidget {
  const ScoreCardItem({
    required this.title,
    required this.value,
    required this.color,
    super.key,
  });

  final String title;
  final String value;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          value,
          style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                fontSize: 18,
                fontWeight: FontWeight.w600,
                color: color,
              ),
        ),
        Text(
          title,
          style:
              Theme.of(context).textTheme.labelMedium!.copyWith(color: color),
        ),
      ],
    );
  }
}
