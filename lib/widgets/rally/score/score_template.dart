import 'package:flutter/material.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:gibz_mobileapp/widgets/rally/score/score_card.dart';

class ScoreTemplate extends StatelessWidget {
  const ScoreTemplate({
    required this.cardWidget,
    this.additionalStackChildren,
    this.additionalColumnChildren,
    super.key,
  });

  final Widget cardWidget;
  final List<Widget>? additionalStackChildren;
  final List<Widget>? additionalColumnChildren;

  @override
  Widget build(BuildContext context) {
    final double headerHeight = MediaQuery.of(context).size.height * (4 / 7);
    const double relativeCardWith = 0.8;
    final double absoluteCardWith =
        MediaQuery.of(context).size.width * relativeCardWith;
    const double cardHeight = 130;

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Stack(
          children: [
            Container(
              height: headerHeight + cardHeight,
            ),
            Container(
              height: headerHeight,
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.primary,
                borderRadius: const BorderRadius.vertical(
                  bottom: Radius.circular(20),
                ),
              ),
            ),
            const SafeArea(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: [
                    PageTitle(
                      title: 'GIBZ Rallye',
                      textColor: Colors.white,
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: headerHeight - (cardHeight / 2),
              left: MediaQuery.of(context).size.width *
                  (1 - relativeCardWith) /
                  2,
              child: SizedBox(
                width: absoluteCardWith,
                height: cardHeight,
                child: ScoreCard(
                  child: cardWidget,
                ),
              ),
            ),
            if (additionalStackChildren != null) ...additionalStackChildren!,
          ],
        ),
        if (additionalColumnChildren != null) ...additionalColumnChildren!
      ],
    );
  }
}
