import 'package:flutter/material.dart';

class ScoreBubble extends StatelessWidget {
  const ScoreBubble({
    required this.scoreIncrement,
    required this.size,
    super.key,
  });

  final int scoreIncrement;
  final double size;

  @override
  Widget build(BuildContext context) {
    final fontColor = scoreIncrement > 0
        ? Theme.of(context).colorScheme.primary
        : const Color.fromRGBO(238, 58, 65, 1);
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: size,
          height: size,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white.withAlpha((255 / 3).round()),
          ),
        ),
        Container(
          width: size * 0.76106,
          height: size * 0.76106,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white.withAlpha((255 / 3).round()),
          ),
        ),
        Container(
          width: size * 0.63821,
          height: size * 0.63821,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white,
          ),
        ),
        Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              '$scoreIncrement',
              style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                    fontSize: 40,
                    color: fontColor,
                  ),
            ),
            Text(
              'Punkte',
              style: Theme.of(context)
                  .textTheme
                  .labelLarge!
                  .copyWith(color: fontColor),
            ),
          ],
        ),
      ],
    );
  }
}
