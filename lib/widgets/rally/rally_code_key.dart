import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/rally/cubit/rally_code_cubit.dart';

class RallyCodeKey extends StatelessWidget {
  const RallyCodeKey({
    super.key,
    required this.child,
    required this.keyboardKey,
    this.isEnabled = true,
    this.onTap,
  });

  final Widget child;
  final LogicalKeyboardKey keyboardKey;
  final bool isEnabled;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return FractionallySizedBox(
      widthFactor: 0.3,
      child: AspectRatio(
        aspectRatio: 1.5,
        child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: isEnabled
              ? onTap ??
                  () {
                    // TODO: Check if all three feedbacks should be used (on android)
                    SystemSound.play(SystemSoundType.click);
                    HapticFeedback.selectionClick();
                    Feedback.forTap(context);
                    context.read<RallyCodeCubit>().handleKey(keyboardKey);
                  }
              : null,
          child: Center(
            child: child,
          ),
        ),
      ),
    );
  }
}
