import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class StageAttachmentLink extends StatelessWidget {
  final String title;
  final String url;

  const StageAttachmentLink({
    required this.title,
    required this.url,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: GestureDetector(
        onTap: () {
          launchUrl(Uri.parse(url));
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Icon(
                Icons.open_in_new,
                color: Theme.of(context).colorScheme.primary,
                size: 19,
              ),
            ),
            Flexible(
              child: Text(
                title,
                style: Theme.of(context)
                    .textTheme
                    .bodyMedium!
                    .copyWith(color: Theme.of(context).colorScheme.primary),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
