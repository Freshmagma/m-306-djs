part of 'stage_activity_answer_input.dart';

class MultipleChoiceAnswerInput extends StageActivityAnswerInput {
  final MultipleChoiceActivity stageActivity;
  const MultipleChoiceAnswerInput(this.stageActivity, {super.key});

  @override
  State<MultipleChoiceAnswerInput> createState() =>
      _MultipleChoiceAnswerInputState();
}

class _MultipleChoiceAnswerInputState extends State<MultipleChoiceAnswerInput> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StageActivityCubit, List<String>>(
      builder: (context, state) {
        return Material(
          color: Colors.transparent,
          child: Column(
            children: [
              for (StageActivityAnswer answerOption
                  in widget.stageActivity.answers)
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 7),
                  child: GestureDetector(
                    onTap: () {
                      context
                          .read<StageActivityCubit>()
                          .toggleAnswer(answerOption.id);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: state.contains(answerOption.id)
                            ? Theme.of(context).colorScheme.primary
                            : const Color.fromRGBO(207, 212, 222, 1),
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: Row(children: [
                        Checkbox(
                          value: state.contains(answerOption.id),
                          checkColor: Theme.of(context).colorScheme.primary,
                          fillColor: MaterialStateColor.resolveWith(
                            (states) => Colors.white,
                          ),
                          onChanged: (bool? isChecked) {
                            if (isChecked == null) {
                              return;
                            }
                            if (isChecked) {
                              context
                                  .read<StageActivityCubit>()
                                  .addAnswer(answerOption.id);
                            } else {
                              context
                                  .read<StageActivityCubit>()
                                  .removeAnswer(answerOption.id);
                            }
                          },
                        ),
                        Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(
                              top: 5,
                              bottom: 5,
                              right: 20,
                            ),
                            child: Text(
                              answerOption.answerText,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .copyWith(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                  ),
                            ),
                          ),
                        )
                      ]),
                    ),
                  ),
                ),
            ],
          ),
        );
      },
    );
  }
}
