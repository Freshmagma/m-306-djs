part of 'stage_activity_answer_input.dart';

class TextInputAnswerInput extends StageActivityAnswerInput {
  final TextInputActivity stageActivity;

  const TextInputAnswerInput(this.stageActivity, {super.key});

  @override
  State<TextInputAnswerInput> createState() => _TextInputAnswerInputState();
}

class _TextInputAnswerInputState extends State<TextInputAnswerInput> {
  final _answerController = TextEditingController();

  @override
  void dispose() {
    _answerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _answerController,
      decoration: InputDecoration(
        hintText: 'Antwort eingeben...',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
      ),
      onChanged: (input) {
        context.read<StageActivityCubit>().setAnswers([_answerController.text]);
      },
    );
  }
}
