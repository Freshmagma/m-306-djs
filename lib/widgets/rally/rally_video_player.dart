import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/rally/cubit/stage_video_duration_cubit.dart';
import 'package:gibz_mobileapp/widgets/rally/custom_video_player_controls.dart';
import 'package:video_player/video_player.dart';

class RallyVideoPlayer extends StatefulWidget {
  final String title;
  final String videoUrl;

  const RallyVideoPlayer({
    required this.title,
    required this.videoUrl,
    Key? key,
  }) : super(key: key);

  @override
  RallyVideoPlayerState createState() => RallyVideoPlayerState();
}

class RallyVideoPlayerState extends State<RallyVideoPlayer> {
  late VideoPlayerController _controller;
  late Future<bool> _initializeVideoPlayerFuture;
  late ChewieController _chewieController;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.networkUrl(Uri.parse(widget.videoUrl));
    _initializeVideoPlayerFuture = _controller.initialize().then((_) {
      final videoDuration = _controller.value.duration;
      context.read<StageVideoDurationCubit>().setDuration(videoDuration);

      return ChewieController(
        autoInitialize: true,
        videoPlayerController: _controller,
        autoPlay: false,
        looping: false,
        showControlsOnInitialize: true,
        showOptions: false,
        allowPlaybackSpeedChanging: false,
        deviceOrientationsOnEnterFullScreen: [
          DeviceOrientation.portraitUp,
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight,
        ],
        deviceOrientationsAfterFullScreen: [
          DeviceOrientation.portraitUp,
        ],
        customControls: CustomVideoPlayerControls(
          videoPlayerController: _controller,
        ),
      );
    }).then((chewieController) {
      _chewieController = chewieController;
      return true;
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _chewieController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _initializeVideoPlayerFuture,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.hasData) {
          return AspectRatio(
            aspectRatio: _controller.value.aspectRatio,
            child: Chewie(controller: _chewieController),
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}
