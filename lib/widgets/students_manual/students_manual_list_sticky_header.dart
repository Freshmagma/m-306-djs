import 'package:flutter/material.dart';

class StudentsManualListStickyHeader extends StatelessWidget {
  const StudentsManualListStickyHeader({super.key, required this.headerChar});

  final String headerChar;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 30),
      child: IgnorePointer(
        child: ListTile(
          title: Text(
            headerChar,
            style: DefaultTextStyle.of(context).style.copyWith(
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                ),
          ),
          visualDensity: VisualDensity.compact,
          contentPadding: const EdgeInsets.symmetric(
            horizontal: 0,
            vertical: 0,
          ),
        ),
      ),
    );
  }
}
