import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/students_manual/cubit/students_manual_article_filter_cubit.dart';

class StudentsManualIndexHeader extends StatefulWidget {
  const StudentsManualIndexHeader({super.key, required this.isSearchEnabled});

  final bool isSearchEnabled;

  @override
  State<StudentsManualIndexHeader> createState() =>
      _StudentsManualIndexHeaderState();
}

class _StudentsManualIndexHeaderState extends State<StudentsManualIndexHeader> {
  final _filterTextController = TextEditingController();

  @override
  void initState() {
    _filterTextController.addListener(_updateFilter);
    super.initState();
  }

  @override
  void dispose() {
    _filterTextController.dispose();
    super.dispose();
  }

  _updateFilter() {
    final filterText = _filterTextController.text;
    context.read<StudentsManualArticleFilterCubit>().updateFilter(filterText);
  }

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      automaticallyImplyLeading: false,
      toolbarHeight: 95,
      backgroundColor: Theme.of(context).colorScheme.primary,
      centerTitle: false,
      pinned: true,
      titleSpacing: 30,
      title: Text(
        'Handbuch',
        style: DefaultTextStyle.of(context).style.copyWith(
              color: Colors.white,
              fontSize: 27,
              fontWeight: FontWeight.w600,
            ),
      ),
      bottom: PreferredSize(
          preferredSize: const Size.fromHeight(58),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 30),
            child: TextField(
              controller: _filterTextController,
              enabled: widget.isSearchEnabled,
              style: DefaultTextStyle.of(context).style.copyWith(fontSize: 14),
              decoration: InputDecoration(
                prefixIcon: const Icon(Icons.search),
                prefixIconConstraints: const BoxConstraints(
                  minWidth: 40,
                  maxWidth: 80,
                  maxHeight: 20,
                ),
                prefixIconColor: Theme.of(context).colorScheme.tertiary,
                hintText: 'Suchen...',
                isDense: true,
                suffixIcon: GestureDetector(
                  onTap: _filterTextController.clear,
                  child: const Icon(Icons.clear_rounded),
                ),
                suffixIconConstraints: const BoxConstraints(minWidth: 40),
              ),
            ),
          )),
    );
  }
}
