import 'package:flutter/material.dart';

class DashboardHeaderDelegate extends SliverPersistentHeaderDelegate {
  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      height: 190,
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/header_bw.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [Color.fromRGBO(0, 119, 192, 1), Colors.transparent],
            stops: [0.1, 2],
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
          ),
        ),
        padding: const EdgeInsets.only(
          left: 30,
          bottom: 25,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: Text(
                'Willkommen zurück!',
                style: DefaultTextStyle.of(context).style.copyWith(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                    ),
              ),
            ),
            RichText(
              text: TextSpan(
                  text: 'GIBZ ',
                  style: DefaultTextStyle.of(context).style.copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        height: 1.2,
                      ),
                  children: const [
                    TextSpan(
                      text: 'Gewerblich-industrielles\n',
                      style: TextStyle(
                        fontWeight: FontWeight.w300,
                      ),
                      children: [
                        TextSpan(text: 'Bildungszentrum Zug'),
                      ],
                    )
                  ]),
            ),
            // Align(
            //   alignment: Alignment.topLeft,
            //   child: SvgPicture.asset(
            //     'assets/images/header_logo.svg',
            //     colorFilter: const ColorFilter.mode(
            //       Colors.white,
            //       BlendMode.srcIn,
            //     ),
            //     height: 34,
            //     fit: BoxFit.contain,
            //   ),
            // ),
          ],
        ),
      ),
    );
  }

  @override
  double get maxExtent => 190;

  @override
  double get minExtent => 190;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
