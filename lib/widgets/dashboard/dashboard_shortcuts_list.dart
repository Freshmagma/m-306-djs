import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/models/dashboard/shortcut.dart';
import 'package:gibz_mobileapp/state_management/profile_picture/profile_picture_bloc.dart';
import 'package:gibz_mobileapp/widgets/dashboard/dashboard_shortcut_item.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DashboardShortcutsList extends StatelessWidget {
  const DashboardShortcutsList({super.key});

  static const shortcuts = [
    // Since the profile picture feature can only be launched using a deep link,
    // this temporary (!) shortcut might be used for development purposes.

    // Shortcut(
    //   title: 'Profilbild TEST',
    //   iconPath: 'assets/icons/dashboard/camera.svg',
    //   url: '/profile-picture/872a76f6-383c-11ee-aca8-42010aac0005',
    // ),
    Shortcut(
      title: 'Menüplan',
      iconPath: 'assets/icons/dashboard/meal_information.svg',
      url: '/meal-information',
    ),
    Shortcut(
      title: 'Rallye',
      iconPath: 'assets/icons/dashboard/rally.svg',
      url: '/gibzrally/start',
    ),
    Shortcut(
      title: 'Parking',
      iconPath: 'assets/icons/dashboard/parking_information.svg',
      url: '/parking-information',
    ),
    Shortcut(
      title: 'Handbuch',
      iconPath: 'assets/icons/menu_studentsManual.svg',
      url: '/students-manual',
    ),
    Shortcut(
      title: 'Impressum',
      iconPath: 'assets/icons/dashboard/imprint.svg',
      url: '/imprint',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfilePictureBloc, ProfilePictureState>(
      builder: (context, state) {
        return FutureBuilder(
          future: _getShortcuts(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const SizedBox.shrink();
            }
            final shortcuts = snapshot.data!;
            return SizedBox(
              height: 100,
              child: ListView.separated(
                scrollDirection: Axis.horizontal,
                separatorBuilder: (context, index) {
                  return const SizedBox(width: 12);
                },
                itemBuilder: (context, index) {
                  return Padding(
                    padding: EdgeInsets.only(
                      left: index == 0 ? 30 : 0,
                      right: index == shortcuts.length - 1 ? 30 : 0,
                    ),
                    child: DashboardShortcutItem(
                      shortcut: shortcuts[index],
                    ),
                  );
                },
                itemCount: shortcuts.length,
              ),
            );
          },
        );
      },
    );
  }

  Future<List<Shortcut>> _getShortcuts() async {
    final shortcuts = [...DashboardShortcutsList.shortcuts];

    final profilePictureToken = await _getProfilePictureToken();
    if (profilePictureToken != null) {
      shortcuts.insert(
        0,
        Shortcut(
          title: 'Profilbild',
          iconPath: 'assets/icons/dashboard/camera.svg',
          url: '/profile-picture/$profilePictureToken',
        ),
      );
    }

    return shortcuts;
  }

  Future<String?> _getProfilePictureToken() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('profile_picture_token');
    return token;
  }
}
