import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/models/meal_information.dart';
import 'package:gibz_mobileapp/widgets/meal_information/price_pill.dart';
import 'package:go_router/go_router.dart';

class DashboardMealItem extends StatelessWidget {
  const DashboardMealItem({required this.meal, super.key});

  final Meal meal;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.go('/meal-information/${meal.id}');
      },
      child: Container(
        height: 130,
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.primary,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Stack(
          children: [
            Positioned(
              top: 15,
              right: 18,
              bottom: 0,
              child: SvgPicture.asset(
                'assets/icons/meal_information/background.svg',
                alignment: Alignment.bottomRight,
              ),
            ),
            Positioned.fill(
              child: Padding(
                padding: const EdgeInsets.all(18),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    PricePill(
                      internalPrice: meal.priceInternal,
                      externalPrice: meal.priceExternal,
                      lowerPriceOnly: true,
                    ),
                    Text(
                      '${meal.menuComponents[0]} ${meal.menuComponents[1]}',
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
