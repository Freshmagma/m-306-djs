import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/models/dashboard/shortcut.dart';
import 'package:go_router/go_router.dart';

class DashboardShortcutItem extends StatelessWidget {
  const DashboardShortcutItem({required this.shortcut, super.key});

  final Shortcut shortcut;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: shortcut.url.isNotEmpty
          ? () {
              context.go(shortcut.url);
            }
          : null,
      child: Container(
        width: 97,
        height: 106,
        padding: const EdgeInsets.only(top: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SvgPicture.asset(
              shortcut.iconPath,
              width: 37,
              height: 38,
              fit: BoxFit.contain,
              colorFilter: ColorFilter.mode(
                Theme.of(context).colorScheme.primary,
                BlendMode.srcIn,
              ),
            ),
            Text(
              shortcut.title,
              style: Theme.of(context).textTheme.titleSmall,
            ),
          ],
        ),
      ),
    );
  }
}
