import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:gibz_mobileapp/data/repositories/students_manual/article_repository.dart';
import 'package:gibz_mobileapp/utilities/default_markdown_stylesheet.dart';
import 'package:gibz_mobileapp/widgets/header_back_button.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:go_router/go_router.dart';
import 'package:url_launcher/url_launcher.dart';

class StudentsManualDetailPage extends StatelessWidget {
  const StudentsManualDetailPage({required this.articleId, super.key});

  final String articleId;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: context.read<ArticleRepository>().getArticle(articleId),
        builder: (context, snapshot) {
          final title = snapshot.hasData ? snapshot.data!.title : 'Handbuch';
          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: 200,
                color: Theme.of(context).colorScheme.primary,
                child: SafeArea(
                  bottom: false,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        PageTitle(
                          title: title,
                          textColor: Colors.white,
                        ),
                        HeaderBackButton(
                          onTap: context.pop,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 20 + 92),
                      child: snapshot.hasData
                          ? MarkdownBody(
                              data: snapshot.data!.content,
                              styleSheet: defaultMarkdownStylesheet,
                              onTapLink: (text, href, title) {
                                if (href != null && href.isNotEmpty) {
                                  launchUrl(Uri.parse(href));
                                }
                              },
                            )
                          : Center(
                              child: Text(
                                'Der gewünschte Artikel ist leider nicht verfügbar.',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyMedium!
                                    .copyWith(fontStyle: FontStyle.italic),
                                textAlign: TextAlign.center,
                              ),
                            ),
                    ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
