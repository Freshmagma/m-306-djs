import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/state_management/rally/rally_bloc.dart';
import 'package:gibz_mobileapp/widgets/markdown_text.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:go_router/go_router.dart';

class PendingStageArrivalPage extends StatelessWidget {
  const PendingStageArrivalPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<RallyBloc, RallyState>(
        listener: (context, state) {
          context.go('/gibzrally');
        },
        builder: (context, state) {
          if (state is! PendingStageArrival) {
            return const SizedBox.shrink();
          }

          final stage = state.stage;

          return SafeArea(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      PageTitle(title: 'GIBZ Rallye'),
                      Row(
                        children: [],
                      ),
                    ],
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(
                              top: 40,
                              bottom: 40,
                            ),
                            child: SvgPicture.asset(
                              'assets/icons/rally/ranging.svg',
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 15),
                              child: Text(
                                stage.preArrivalInformation.title,
                                style:
                                    Theme.of(context).textTheme.headlineMedium,
                              ),
                            ),
                            MarkdownText(
                              data: stage.preArrivalInformation.content,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 30),
                    child: TextButton(
                      onPressed: () {
                        context.read<RallyBloc>().add(SkippedStage());
                      },
                      child: const Text('Station überspringen'),
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
