import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/progress_indicator_cubit.dart';
import 'package:gibz_mobileapp/state_management/rally/rally_bloc.dart';
import 'package:gibz_mobileapp/widgets/rally/score/score_bubble.dart';
import 'package:gibz_mobileapp/widgets/rally/score/score_card_points_data.dart';
import 'package:gibz_mobileapp/widgets/rally/score/score_template.dart';
import 'package:go_router/go_router.dart';

class ScorePage extends StatelessWidget {
  const ScorePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: false,
      body: BlocConsumer<RallyBloc, RallyState>(
        listener: (context, state) {
          context.go('/gibzrally');
        },
        builder: (context, state) {
          if (state is! DisplayingCurrentScore) {
            return const SizedBox.shrink();
          }

          const double scoreBubbleSize = 226;

          return ScoreTemplate(
            cardWidget: ScoreCardPointsData(state: state),
            additionalStackChildren: [
              Positioned.fill(
                top: -100,
                child: ScoreBubble(
                  scoreIncrement: state.latestPoints,
                  size: scoreBubbleSize,
                ),
              ),
            ],
            additionalColumnChildren: [
              Padding(
                padding: const EdgeInsets.only(
                  left: 30,
                  right: 30,
                  top: 30,
                  bottom: 30 + 92,
                ),
                child: BlocBuilder<ProgressIndicatorCubit, LoadingState>(
                  builder: (context, loadingState) {
                    return ElevatedButton(
                      onPressed:
                          !loadingState.contains(LoadingAspect.loadingNextStage)
                              ? () {
                                  context
                                      .read<RallyBloc>()
                                      .add(NextStageRequested());
                                }
                              : null,
                      child: const Text('Weiter'),
                    );
                  },
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
