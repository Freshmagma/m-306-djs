import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/progress_indicator_cubit.dart';
import 'package:gibz_mobileapp/state_management/rally/cubit/stage_activity_cubit.dart';
import 'package:gibz_mobileapp/state_management/rally/rally_bloc.dart';
import 'package:gibz_mobileapp/widgets/header_back_button.dart';
import 'package:gibz_mobileapp/widgets/markdown_text.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:gibz_mobileapp/widgets/rally/rally_card.dart';
import 'package:gibz_mobileapp/widgets/rally/stage_activity/stage_activity_answer_input.dart';
import 'package:go_router/go_router.dart';

class StageActivityPage extends StatefulWidget {
  const StageActivityPage({super.key});

  @override
  State<StageActivityPage> createState() => _StageActivityPageState();
}

class _StageActivityPageState extends State<StageActivityPage> {
  List<String?> answers = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      extendBody: false,
      body: BlocConsumer<RallyBloc, RallyState>(
        listener: (context, state) {
          context.go('/gibzrally');
        },
        builder: (context, state) {
          if (state is! PendingStageActivity) {
            return const SizedBox.shrink();
          }

          return SafeArea(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const PageTitle(title: 'GIBZ Rallye'),
                      HeaderBackButton(onTap: () {
                        context
                            .read<RallyBloc>()
                            .add(StageInformationRequested(state.stage));
                      }),
                    ],
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: RallyCard(
                        child: SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.all(15),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  state.stage.stageActivity.title,
                                  style:
                                      Theme.of(context).textTheme.headlineSmall,
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 15),
                                  child: MarkdownText(
                                    data: state.stage.stageActivity.task,
                                  ),
                                ),
                                BlocProvider(
                                  create: (context) => StageActivityCubit(),
                                  child: BlocListener<StageActivityCubit,
                                      List<String>>(
                                    listener: (context, state) {
                                      setState(() {
                                        answers = state;
                                      });
                                    },
                                    child: StageActivityAnswerInput
                                        .forStageActivity(
                                            state.stage.stageActivity),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 30),
                        child:
                            BlocBuilder<ProgressIndicatorCubit, LoadingState>(
                          builder: (context, loadingState) {
                            return ElevatedButton(
                              onPressed: !loadingState.contains(LoadingAspect
                                          .submittingStageActivityAnswer) &&
                                      answers.isNotEmpty &&
                                      answers
                                          .every((a) => a?.isNotEmpty ?? false)
                                  ? () {
                                      context
                                          .read<RallyBloc>()
                                          .add(AnswerSubmissionRequested(
                                            state.stage.stageActivity.id,
                                            answers,
                                          ));
                                    }
                                  : null,
                              child: const Text('Absenden'),
                            );
                          },
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
