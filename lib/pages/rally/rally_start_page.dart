import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/state_management/rally/rally_bloc.dart';
import 'package:gibz_mobileapp/widgets/header_back_button.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:go_router/go_router.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RallyStartPage extends StatelessWidget {
  const RallyStartPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<RallyBloc, RallyState>(
        listener: (context, state) {
          context.go('/gibzrally');
        },
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const PageTitle(title: 'GIBZ Rallye'),
                    HeaderBackButton(onTap: () {
                      if (context.canPop()) {
                        context.pop();
                      }
                    }),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 60,
                        bottom: 60,
                      ),
                      child: SizedBox(
                        width: 180,
                        height: 180,
                        child: SvgPicture.asset(
                          'assets/icons/dashboard/rally.svg',
                          fit: BoxFit.contain,
                          // width: MediaQuery.of(context).size.shortestSide - 200,
                        ),
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 15),
                          child: Text(
                            'GIBZ Rallye',
                            style: Theme.of(context).textTheme.headlineMedium,
                          ),
                        ),
                        Text(
                          'Mit der GIBZ Rallye können Sie den Campus des Gewerblich-industriellen Bildungszentrums Zug erkunden - multimedial und interaktiv.',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ],
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    FutureBuilder(
                      future: _hasOngoingRally(),
                      builder: (context, snapshot) {
                        if (snapshot.hasError ||
                            !snapshot.hasData ||
                            snapshot.data != true) {
                          return const SizedBox.shrink();
                        }

                        return TextButton(
                            onPressed: () {
                              context
                                  .read<RallyBloc>()
                                  .add(ResumeRallyRequested());
                            },
                            child: const Text('Letzte Rallye fortsetzen'));
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 10,
                        bottom: 30,
                      ),
                      child: ElevatedButton(
                        onPressed: () {
                          context.go('/gibzrally/code');
                        },
                        child: const Text('GIBZ Rallye starten'),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> _hasOngoingRally() async {
    final prefs = await SharedPreferences.getInstance();
    final rallyId = prefs.getString("rallyId");
    final participatingPartyId = prefs.getString("participatingPartyId");
    final stageId = prefs.getString("lastCompletedStageId");

    return (rallyId?.isNotEmpty ?? false) &&
        (participatingPartyId?.isNotEmpty ?? false) &&
        (stageId?.isNotEmpty ?? false);
  }
}
