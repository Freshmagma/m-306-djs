import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/meal_information/meal_information_bloc.dart';
import 'package:gibz_mobileapp/widgets/dashboard/dashboard_header_delegate.dart';
import 'package:gibz_mobileapp/widgets/dashboard/dashboard_meal_item.dart';
import 'package:gibz_mobileapp/widgets/dashboard/dashboard_shortcuts_list.dart';
import 'package:gibz_mobileapp/widgets/parking_information.dart/parking_info_availability_card.dart';
import 'package:go_router/go_router.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverPersistentHeader(
            delegate: DashboardHeaderDelegate(),
            pinned: true,
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(30),
              child: Text(
                'Shortcuts',
                style: Theme.of(context).textTheme.titleLarge,
              ),
            ),
          ),
          const SliverToBoxAdapter(
            child: DashboardShortcutsList(),
          ),
          SliverToBoxAdapter(
            child: GestureDetector(
              onTap: () {
                context.go('/meal-information');
              },
              child: Padding(
                padding: const EdgeInsets.all(30),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Menüplan',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    Text(
                      'Mehr anzeigen',
                      style: Theme.of(context).textTheme.bodyMedium,
                    ),
                  ],
                ),
              ),
            ),
          ),
          BlocBuilder<MealInformationBloc, MealInformationState>(
            builder: (context, state) {
              if (state is MealInformationAvailable &&
                  state.dashboardMeals != null &&
                  state.dashboardMeals!.isNotEmpty) {
                return SliverPadding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  sliver: SliverList.separated(
                    separatorBuilder: (context, index) {
                      return const SizedBox(height: 12);
                    },
                    itemBuilder: (context, index) {
                      return DashboardMealItem(
                        meal: state.dashboardMeals![index],
                      );
                    },
                    itemCount: state.dashboardMeals!.length,
                  ),
                );
              }

              if (state is NoMealInformationAvailable ||
                  state is MealInformationAvailable &&
                      (state.dashboardMeals == null ||
                          state.dashboardMeals!.isEmpty)) {
                return SliverPadding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  sliver: SliverToBoxAdapter(
                    child: Text(
                      'Aktuell sind für heute keine Informationen zum Menüplan im Restaurant Treff verfügbar.',
                      style: Theme.of(context).textTheme.bodySmall!.copyWith(
                            color: Theme.of(context)
                                .colorScheme
                                .tertiary
                                .withAlpha(100),
                          ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                );
              }

              return const SliverToBoxAdapter(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            },
          ),
          SliverToBoxAdapter(
            child: GestureDetector(
              onTap: () {
                context.go('/parking-information');
              },
              child: Padding(
                padding: const EdgeInsets.all(30),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Parking',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    Text(
                      'Mehr anzeigen',
                      style: Theme.of(context).textTheme.bodyMedium,
                    ),
                  ],
                ),
              ),
            ),
          ),
          SliverPadding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            sliver: SliverToBoxAdapter(
              child: GestureDetector(
                onTap: () {
                  context.go('/parking-information');
                },
                child: const ParkingInfoAvailabilityCard(),
              ),
            ),
          ),
          const SliverToBoxAdapter(
            child: SizedBox(height: 123),
          ),
        ],
      ),
    );
  }
}
