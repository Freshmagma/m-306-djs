import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/profile_picture/profile_picture_bloc.dart';
import 'package:gibz_mobileapp/widgets/header_back_button.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:gibz_mobileapp/widgets/profile_picture/face_mask.dart';
import 'package:go_router/go_router.dart';

class CameraPage extends StatefulWidget {
  const CameraPage({required this.token, super.key});

  final String token;

  @override
  State<CameraPage> createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  late CameraController controller;
  late Future<void> future;

  XFile? file;

  @override
  void initState() {
    super.initState();
    future = availableCameras().then((cameras) {
      return cameras
          .where((camera) => camera.lensDirection == CameraLensDirection.front)
          .toList()
          .firstOrNull;
    }).then((camera) {
      if (camera != null) {
        controller = CameraController(
          camera,
          ResolutionPreset.high,
          enableAudio: false,
        );
      }
      controller.initialize();
    }).then((value) {
      return Future.delayed(const Duration(milliseconds: 500));
    });
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;

    return BlocListener<ProfilePictureBloc, ProfilePictureState>(
      listener: (context, state) {
        if (state is PendingPictureApproval) {
          context.go(
            '/profile-picture/${widget.token}/face-instructions/camera/approval',
            extra: state.file,
          );
        }
      },
      child: Scaffold(
        body: Stack(
          children: [
            FutureBuilder(
              future: future,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  final size = MediaQuery.of(context).size;
                  var scale = size.aspectRatio * controller.value.aspectRatio;
                  if (scale < 1) scale = 1 / scale;
                  return Transform.scale(
                    scale: scale,
                    child: CameraPreview(controller),
                  );
                }
                return Align(
                  child: Container(
                      width: 180,
                      margin: EdgeInsets.only(
                          bottom: (height * 0.5) - (height * (1 / 4))),
                      child: const Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(bottom: 20),
                            child: CircularProgressIndicator(),
                          ),
                          Text(
                            'Kamera wird geladen...',
                            textAlign: TextAlign.center,
                          ),
                        ],
                      )),
                );
              },
            ),
            FaceMask(height: height),
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const PageTitle(
                          title: 'Profilbild',
                          textColor: Colors.white,
                        ),
                        HeaderBackButton(
                          onTap: () {
                            context.go(
                                '/profile-picture/${widget.token}/face-instructions');
                          },
                          color: Colors.white,
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 30),
                      child: FutureBuilder(
                        future: future,
                        builder: (context, snapshot) => ElevatedButton(
                          onPressed:
                              snapshot.connectionState == ConnectionState.done
                                  ? () async {
                                      controller.setFlashMode(FlashMode.off);
                                      controller.takePicture().then(
                                        (file) {
                                          setState(() {
                                            this.file = file;
                                            context
                                                .read<ProfilePictureBloc>()
                                                .add(PictureTaken(file));
                                          });
                                        },
                                      );
                                    }
                                  : null,
                          child: const Text('Foto aufnehmen'),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
