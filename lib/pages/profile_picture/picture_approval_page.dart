import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/profile_picture/cubit/profile_picture_approval_cubit.dart';
import 'package:gibz_mobileapp/state_management/profile_picture/profile_picture_bloc.dart';
import 'package:gibz_mobileapp/widgets/header_back_button.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:gibz_mobileapp/widgets/profile_picture/profile_picture_approval_checkbox.dart';
import 'package:gibz_mobileapp/widgets/profile_picture/profile_picture_preview.dart';
import 'package:gibz_mobileapp/widgets/profile_picture/profile_picture_upload_button.dart';
import 'package:go_router/go_router.dart';

class PictureApprovalPage extends StatefulWidget {
  const PictureApprovalPage({
    required this.token,
    required this.file,
    super.key,
  });

  final String token;
  final XFile file;

  @override
  State<PictureApprovalPage> createState() => _PictureApprovalPageState();
}

class _PictureApprovalPageState extends State<PictureApprovalPage> {
  bool? isFaceVisible = false;
  bool? isLightingOk = false;
  @override
  Widget build(BuildContext context) {
    return BlocListener<ProfilePictureBloc, ProfilePictureState>(
      listener: (context, state) {
        if (state is ProfilePictureAccepted) {
          context.go('/profile-picture/${widget.token}/accepted');
        } else if (state is ProfilePictureRejected) {
          context.go('/profile-picture/${widget.token}/rejected');
        }
      },
      child: Scaffold(
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: BlocProvider(
              create: (context) => ProfilePictureApprovalCubit(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const PageTitle(title: 'Profilbild'),
                      HeaderBackButton(onTap: () {
                        context.go(
                            '/profile-picture/${widget.token}/face-instructions/camera');
                      }),
                    ],
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: ProfilePicturePreview(path: widget.file.path),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 15),
                        child: Text(
                          'Profilbild bestätigen',
                          style: Theme.of(context).textTheme.headlineMedium,
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text(
                          'Überprüfen Sie, ob Sie mit dem gemachten Foto zufrieden sind. Bestätigen Sie anschliessend die Qualität des Fotos.',
                        ),
                      ),
                      const Column(
                        children: [
                          ProfilePictureApprovalCheckbox(
                            labelText: 'Mein Gesicht ist vollständig sichtbar',
                            criteria:
                                ProfilePictureApprovalCriteria.facePosition,
                          ),
                          ProfilePictureApprovalCheckbox(
                            labelText:
                                'Mein Gesicht ist gleichmässig ausgeleuchtet',
                            criteria: ProfilePictureApprovalCriteria.lighting,
                          ),
                        ],
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 10,
                      bottom: 30,
                    ),
                    child: ProfilePictureUploadButton(
                      token: widget.token,
                      file: widget.file,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
