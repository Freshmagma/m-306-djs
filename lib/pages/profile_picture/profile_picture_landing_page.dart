import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/state_management/profile_picture/profile_picture_bloc.dart';
import 'package:gibz_mobileapp/widgets/header_back_button.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:go_router/go_router.dart';

class ProfilePictureLandingPage extends StatelessWidget {
  const ProfilePictureLandingPage({required this.token, super.key});

  final String token;

  @override
  Widget build(BuildContext context) {
    context
        .read<ProfilePictureBloc>()
        .add(ProfilePictureProcessLaunched(token));
    return BlocListener<ProfilePictureBloc, ProfilePictureState>(
      listener: (context, state) {
        if (state is PendingShutter) {
          context.go('/profile-picture/$token/face-instructions');
        }
      },
      child: Scaffold(
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const PageTitle(title: 'Profilbild'),
                    HeaderBackButton(
                      onTap: () {
                        context.go('/');
                      },
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 40),
                      child: SvgPicture.asset(
                          'assets/icons/profile_picture/camera.svg'),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 15),
                          child: Text(
                            'Profilbild aufnehmen',
                            style: Theme.of(context).textTheme.headlineMedium,
                          ),
                        ),
                        Text(
                          'Sie können die Kamera Ihres Smartphones verwenden, um das eigene Profilbild zu erstellen. Gewähren Sie der GIBZ App dafür bitte den Zugriff auf die Kamera.',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ],
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 30),
                  child: ElevatedButton(
                    onPressed: () {
                      context
                          .read<ProfilePictureBloc>()
                          .add(CameraPermissionGrantRequested());
                    },
                    child: const Text(
                      'Zugriff gewähren',
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
