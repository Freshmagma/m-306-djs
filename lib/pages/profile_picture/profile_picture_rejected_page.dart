import 'package:flutter/material.dart';
import 'package:gibz_mobileapp/widgets/profile_picture/profile_picture_final_page_template.dart';

class ProfilePictureRejectedPage extends StatelessWidget {
  const ProfilePictureRejectedPage({required this.token, super.key});

  final String token;

  @override
  Widget build(BuildContext context) {
    return ProfilePictureFinalPageTemplate(
      color: const Color.fromRGBO(239, 62, 66, 1),
      iconName: "rejected.svg",
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: Text(
            'Übermittlung fehlgeschlagen',
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(color: Colors.white),
          ),
        ),
        Text(
          'Das Profilbild konnte wegen einem technischen Fehler nicht übermittelt werden.',
          style: Theme.of(context)
              .textTheme
              .bodyMedium!
              .copyWith(color: Colors.white),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10),
          child: Text(
            'Versuchen Sie es bitte später nochmals - danke.',
            style: Theme.of(context)
                .textTheme
                .bodyMedium!
                .copyWith(color: Colors.white),
          ),
        ),
      ],
    );
  }
}
