import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/state_management/profile_picture/profile_picture_bloc.dart';
import 'package:gibz_mobileapp/widgets/header_back_button.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:go_router/go_router.dart';

class FacePositionInstructionsPage extends StatelessWidget {
  const FacePositionInstructionsPage({required this.token, super.key});

  final String token;

  @override
  Widget build(BuildContext context) {
    context
        .read<ProfilePictureBloc>()
        .add(ProfilePictureProcessLaunched(token));
    return BlocListener<ProfilePictureBloc, ProfilePictureState>(
      listener: (context, state) {
        if (state is PendingShutter) {
          context.go('/profile-picture/$token/face-instructions/camera');
        }
      },
      child: Scaffold(
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const PageTitle(title: 'Profilbild'),
                    HeaderBackButton(
                      onTap: () {
                        context.go('/');
                      },
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 40),
                      child: SvgPicture.asset(
                          'assets/icons/profile_picture/face_position.svg'),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 15),
                          child: Text(
                            'Gesicht ausrichten',
                            style: Theme.of(context).textTheme.headlineMedium,
                          ),
                        ),
                        Text(
                          'Positionieren Sie Ihr Gesicht im nächsten Schritt so, dass dieses mittig im markierten Bereich erscheint.',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Text(
                            'Achten Sie darauf, dass Ihr Gesicht gleichmässig ausgeleuchtet und nicht verdeckt ist. Legen Sie dafür eine allfällige Kopfbedeckung ab.',
                            style: Theme.of(context).textTheme.bodyMedium,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 30),
                  child: ElevatedButton(
                    onPressed: () {
                      context
                          .read<ProfilePictureBloc>()
                          .add(FacePositionInstructionsRead());
                    },
                    child: const Text(
                      'Profilbild erstellen',
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
