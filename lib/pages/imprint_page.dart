import 'package:flutter/material.dart';
import 'package:gibz_mobileapp/widgets/header_back_button.dart';
import 'package:gibz_mobileapp/widgets/markdown_text.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:go_router/go_router.dart';

class ImprintPage extends StatelessWidget {
  const ImprintPage({super.key});

  final imprintData =
      '''Die GIBZ App ist für iOS und Android in den jeweiligen Stores gratis zum Download verfügbar.

Die Inhalte und Informationen stammen aus verschiedenen, öffentlich zugänglichen Quellen. Das GIBZ als Betreiberin haftet nicht für Aktualität und Korrektheit der Daten.

## Betrieb

Gewerblich-industrielles Bildungszentrum Zug  
Baarerstrasse 100  
Postfach  
6301 Zug  
[https://www.gibz.ch](https://www.gibz.ch)  
041 728 30 30 

## Idee, Konzept, Realisierung

Peter Gisler

## UI Design

Codelance GmbH

## Entwicklung

Das Backend zur Speicherung, Aufbereitung und Auslieferung verschiedener Daten, ein Web-Frontend zur Verwaltung der Inhalte sowie die mobile Applikation für iOS und Android werden unter der Leitung von Peter Gisler mit ausgewählten Lernenden und Klassen des GIBZ im Rahmen des regulären Berufsschulunterrichts für Informatik mit Fachrichtung Applikationsentwicklung oder in spezifischen Freifächern entwickelt.

In den nachfolgenden Abschnitten sind einige Lernende des GIBZ aufgeführt, welche einen substantiellen Beitrag zur produktiv genutzten GIBZ App beigetragen haben.

### Handbuch für Lernende

Alina Meyer

### Menüplan ZFV

Annika Christen und weitere Lernende

### Parking Informationen

André Schreiber und weitere Lernende

### Profilbilder

Philipp Würsch, Loris Janner, Alessio Seeberger, Tim Arnold, Luis Leuppi, Riccardo Zinniker

## Datenschutz

Die GIBZ App erhebt oder speichert ausschliesslich personenbezogene Daten, welche für den Betrieb der App notwendig sind und deren Nutzung die Anwenderinnen und Anwender zuvor zugestimmt haben.

Die detaillierte [Datenschutzrichtlinie](https://www.termsfeed.com/live/a0b9ca37-83b1-4647-917f-632903f384e8) ist online einsehbar.

In der aktuellen Version werden mit dem Einverständnis der Nutzerinnen und Nutzer folgende Daten erhoben:

### Physischer Standort des Geräts
Während der Nutzung der GIBZ Rallye wird mittels Bluetooth Beacons der physische Standort des Geräts ermittelt. Dieser Standort wird lediglich für die Basisfunktionalität der GIBZ Rallye gespeichert. Bei der Nutzung anderer Funktionen der GIBZ App werden keine Standortdaten erhoben.

### Profilfoto
Die GIBZ App kann zur Erstellung des persönlichen Profilfotos am GIBZ genutzt werden. Dieses Foto wird nach der Erstellung und Überprüfung durch die Nutzerin bzw. den Nutzer an den Server der GIBZ App übermittelt und dort sowie im [schulNetz](/students-manual/08da4ebc-4c34-439f-8147-b668cf5ed692) unter Einhaltung der Schweizer Datenschutzbestimmungen gespeichert.
''';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            height: 200,
            color: Theme.of(context).colorScheme.primary,
            child: SafeArea(
              bottom: false,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const PageTitle(
                      title: 'Impressum',
                      textColor: Colors.white,
                    ),
                    HeaderBackButton(
                      onTap: context.pop,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 20,
                    bottom: 20 + 92,
                  ),
                  child: MarkdownText(data: imprintData),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
