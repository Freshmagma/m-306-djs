import 'package:gibz_mobileapp/models/meal_information.dart';

import 'students_manual.dart';

void initializeMappers() {
  _initializeStudentsManualMappers();
  _initializeMealInformationMappers();
}

void _initializeStudentsManualMappers() {
  ArticleMapper.ensureInitialized();
  AttachmentMapper.ensureInitialized();
  ExternalLinkMapper.ensureInitialized();
  PdfDocumentMapper.ensureInitialized();
  TagMapper.ensureInitialized();
  VideoMapper.ensureInitialized();
}

void _initializeMealInformationMappers() {
  DayMapper.ensureInitialized();
  MealIconMapper.ensureInitialized();
  MealMapper.ensureInitialized();
  NutritionalValueMapper.ensureInitialized();
}
