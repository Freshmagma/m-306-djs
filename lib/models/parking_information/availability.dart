import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

part 'availability.mapper.dart';

@MappableClass()
class Availability extends Equatable with AvailabilityMappable {
  final String id;
  final String status;
  final int free;
  final DateTime moment;

  bool get isOpen => status.toLowerCase() == 'offen';

  @override
  List<Object?> get props => [id];

  const Availability({
    required this.id,
    required this.status,
    required this.free,
    required this.moment,
  });
}
