// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'availability.dart';

class AvailabilityMapper extends ClassMapperBase<Availability> {
  AvailabilityMapper._();

  static AvailabilityMapper? _instance;
  static AvailabilityMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = AvailabilityMapper._());
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'Availability';

  static String _$id(Availability v) => v.id;
  static const Field<Availability, String> _f$id = Field('id', _$id);
  static String _$status(Availability v) => v.status;
  static const Field<Availability, String> _f$status =
      Field('status', _$status);
  static int _$free(Availability v) => v.free;
  static const Field<Availability, int> _f$free = Field('free', _$free);
  static DateTime _$moment(Availability v) => v.moment;
  static const Field<Availability, DateTime> _f$moment =
      Field('moment', _$moment);

  @override
  final Map<Symbol, Field<Availability, dynamic>> fields = const {
    #id: _f$id,
    #status: _f$status,
    #free: _f$free,
    #moment: _f$moment,
  };

  static Availability _instantiate(DecodingData data) {
    return Availability(
        id: data.dec(_f$id),
        status: data.dec(_f$status),
        free: data.dec(_f$free),
        moment: data.dec(_f$moment));
  }

  @override
  final Function instantiate = _instantiate;

  static Availability fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<Availability>(map));
  }

  static Availability fromJson(String json) {
    return _guard((c) => c.fromJson<Availability>(json));
  }
}

mixin AvailabilityMappable {
  String toJson() {
    return AvailabilityMapper._guard((c) => c.toJson(this as Availability));
  }

  Map<String, dynamic> toMap() {
    return AvailabilityMapper._guard((c) => c.toMap(this as Availability));
  }

  AvailabilityCopyWith<Availability, Availability, Availability> get copyWith =>
      _AvailabilityCopyWithImpl(this as Availability, $identity, $identity);
  @override
  String toString() {
    return AvailabilityMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            AvailabilityMapper._guard((c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return AvailabilityMapper._guard((c) => c.hash(this));
  }
}

extension AvailabilityValueCopy<$R, $Out>
    on ObjectCopyWith<$R, Availability, $Out> {
  AvailabilityCopyWith<$R, Availability, $Out> get $asAvailability =>
      $base.as((v, t, t2) => _AvailabilityCopyWithImpl(v, t, t2));
}

abstract class AvailabilityCopyWith<$R, $In extends Availability, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? id, String? status, int? free, DateTime? moment});
  AvailabilityCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _AvailabilityCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, Availability, $Out>
    implements AvailabilityCopyWith<$R, Availability, $Out> {
  _AvailabilityCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<Availability> $mapper =
      AvailabilityMapper.ensureInitialized();
  @override
  $R call({String? id, String? status, int? free, DateTime? moment}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (status != null) #status: status,
        if (free != null) #free: free,
        if (moment != null) #moment: moment
      }));
  @override
  Availability $make(CopyWithData data) => Availability(
      id: data.get(#id, or: $value.id),
      status: data.get(#status, or: $value.status),
      free: data.get(#free, or: $value.free),
      moment: data.get(#moment, or: $value.moment));

  @override
  AvailabilityCopyWith<$R2, Availability, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _AvailabilityCopyWithImpl($value, $cast, t);
}
