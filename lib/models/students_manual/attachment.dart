import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

part 'attachment.mapper.dart';

@MappableClass(discriminatorKey: "type")
abstract class Attachment extends Equatable with AttachmentMappable {
  final String id;
  final String title;
  final String url;

  @override
  List<Object?> get props => [id];

  const Attachment({required this.id, required this.title, required this.url});

  static const fromMap = AttachmentMapper.fromMap;
  static const fromJson = AttachmentMapper.fromJson;
}
