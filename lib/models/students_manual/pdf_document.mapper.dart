// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'pdf_document.dart';

class PdfDocumentMapper extends SubClassMapperBase<PdfDocument> {
  PdfDocumentMapper._();

  static PdfDocumentMapper? _instance;
  static PdfDocumentMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = PdfDocumentMapper._());
      AttachmentMapper.ensureInitialized().addSubMapper(_instance!);
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'PdfDocument';

  static String _$id(PdfDocument v) => v.id;
  static const Field<PdfDocument, dynamic> _f$id = Field('id', _$id);
  static String _$title(PdfDocument v) => v.title;
  static const Field<PdfDocument, dynamic> _f$title = Field('title', _$title);
  static String _$url(PdfDocument v) => v.url;
  static const Field<PdfDocument, dynamic> _f$url = Field('url', _$url);

  @override
  final Map<Symbol, Field<PdfDocument, dynamic>> fields = const {
    #id: _f$id,
    #title: _f$title,
    #url: _f$url,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = "pdf";
  @override
  late final ClassMapperBase superMapper = AttachmentMapper.ensureInitialized();

  static PdfDocument _instantiate(DecodingData data) {
    return PdfDocument(
        id: data.dec(_f$id), title: data.dec(_f$title), url: data.dec(_f$url));
  }

  @override
  final Function instantiate = _instantiate;

  static PdfDocument fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<PdfDocument>(map));
  }

  static PdfDocument fromJson(String json) {
    return _guard((c) => c.fromJson<PdfDocument>(json));
  }
}

mixin PdfDocumentMappable {
  String toJson() {
    return PdfDocumentMapper._guard((c) => c.toJson(this as PdfDocument));
  }

  Map<String, dynamic> toMap() {
    return PdfDocumentMapper._guard((c) => c.toMap(this as PdfDocument));
  }

  PdfDocumentCopyWith<PdfDocument, PdfDocument, PdfDocument> get copyWith =>
      _PdfDocumentCopyWithImpl(this as PdfDocument, $identity, $identity);
  @override
  String toString() {
    return PdfDocumentMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            PdfDocumentMapper._guard((c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return PdfDocumentMapper._guard((c) => c.hash(this));
  }
}

extension PdfDocumentValueCopy<$R, $Out>
    on ObjectCopyWith<$R, PdfDocument, $Out> {
  PdfDocumentCopyWith<$R, PdfDocument, $Out> get $asPdfDocument =>
      $base.as((v, t, t2) => _PdfDocumentCopyWithImpl(v, t, t2));
}

abstract class PdfDocumentCopyWith<$R, $In extends PdfDocument, $Out>
    implements AttachmentCopyWith<$R, $In, $Out> {
  @override
  $R call({dynamic id, dynamic title, dynamic url});
  PdfDocumentCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _PdfDocumentCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, PdfDocument, $Out>
    implements PdfDocumentCopyWith<$R, PdfDocument, $Out> {
  _PdfDocumentCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<PdfDocument> $mapper =
      PdfDocumentMapper.ensureInitialized();
  @override
  $R call({Object? id = $none, Object? title = $none, Object? url = $none}) =>
      $apply(FieldCopyWithData({
        if (id != $none) #id: id,
        if (title != $none) #title: title,
        if (url != $none) #url: url
      }));
  @override
  PdfDocument $make(CopyWithData data) => PdfDocument(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      url: data.get(#url, or: $value.url));

  @override
  PdfDocumentCopyWith<$R2, PdfDocument, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _PdfDocumentCopyWithImpl($value, $cast, t);
}
