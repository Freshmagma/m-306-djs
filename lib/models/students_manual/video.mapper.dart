// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'video.dart';

class VideoMapper extends SubClassMapperBase<Video> {
  VideoMapper._();

  static VideoMapper? _instance;
  static VideoMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = VideoMapper._());
      AttachmentMapper.ensureInitialized().addSubMapper(_instance!);
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'Video';

  static String _$id(Video v) => v.id;
  static const Field<Video, dynamic> _f$id = Field('id', _$id);
  static String _$title(Video v) => v.title;
  static const Field<Video, dynamic> _f$title = Field('title', _$title);
  static String _$url(Video v) => v.url;
  static const Field<Video, dynamic> _f$url = Field('url', _$url);

  @override
  final Map<Symbol, Field<Video, dynamic>> fields = const {
    #id: _f$id,
    #title: _f$title,
    #url: _f$url,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = "video";
  @override
  late final ClassMapperBase superMapper = AttachmentMapper.ensureInitialized();

  static Video _instantiate(DecodingData data) {
    return Video(
        id: data.dec(_f$id), title: data.dec(_f$title), url: data.dec(_f$url));
  }

  @override
  final Function instantiate = _instantiate;

  static Video fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<Video>(map));
  }

  static Video fromJson(String json) {
    return _guard((c) => c.fromJson<Video>(json));
  }
}

mixin VideoMappable {
  String toJson() {
    return VideoMapper._guard((c) => c.toJson(this as Video));
  }

  Map<String, dynamic> toMap() {
    return VideoMapper._guard((c) => c.toMap(this as Video));
  }

  VideoCopyWith<Video, Video, Video> get copyWith =>
      _VideoCopyWithImpl(this as Video, $identity, $identity);
  @override
  String toString() {
    return VideoMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            VideoMapper._guard((c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return VideoMapper._guard((c) => c.hash(this));
  }
}

extension VideoValueCopy<$R, $Out> on ObjectCopyWith<$R, Video, $Out> {
  VideoCopyWith<$R, Video, $Out> get $asVideo =>
      $base.as((v, t, t2) => _VideoCopyWithImpl(v, t, t2));
}

abstract class VideoCopyWith<$R, $In extends Video, $Out>
    implements AttachmentCopyWith<$R, $In, $Out> {
  @override
  $R call({dynamic id, dynamic title, dynamic url});
  VideoCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _VideoCopyWithImpl<$R, $Out> extends ClassCopyWithBase<$R, Video, $Out>
    implements VideoCopyWith<$R, Video, $Out> {
  _VideoCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<Video> $mapper = VideoMapper.ensureInitialized();
  @override
  $R call({Object? id = $none, Object? title = $none, Object? url = $none}) =>
      $apply(FieldCopyWithData({
        if (id != $none) #id: id,
        if (title != $none) #title: title,
        if (url != $none) #url: url
      }));
  @override
  Video $make(CopyWithData data) => Video(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      url: data.get(#url, or: $value.url));

  @override
  VideoCopyWith<$R2, Video, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
      _VideoCopyWithImpl($value, $cast, t);
}
