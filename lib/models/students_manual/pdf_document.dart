import 'package:dart_mappable/dart_mappable.dart';
import 'attachment.dart';

part 'pdf_document.mapper.dart';

@MappableClass(discriminatorValue: "pdf")
class PdfDocument extends Attachment with PdfDocumentMappable {
  const PdfDocument({required id, required title, required url})
      : super(id: id, title: title, url: url);

  static const fromMap = PdfDocumentMapper.fromMap;
  static const fromJson = PdfDocumentMapper.fromJson;
}
