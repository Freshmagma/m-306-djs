import 'package:dart_mappable/dart_mappable.dart';
import 'attachment.dart';

part 'video.mapper.dart';

@MappableClass(discriminatorValue: "video")
class Video extends Attachment with VideoMappable {
  const Video({required id, required title, required url})
      : super(id: id, title: title, url: url);

  static const fromMap = VideoMapper.fromMap;
  static const fromJson = VideoMapper.fromJson;
}
