// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'article.dart';

class ArticleMapper extends ClassMapperBase<Article> {
  ArticleMapper._();

  static ArticleMapper? _instance;
  static ArticleMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = ArticleMapper._());
      TagMapper.ensureInitialized();
      AttachmentMapper.ensureInitialized();
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'Article';

  static String _$id(Article v) => v.id;
  static const Field<Article, String> _f$id = Field('id', _$id);
  static String _$title(Article v) => v.title;
  static const Field<Article, String> _f$title = Field('title', _$title);
  static String _$content(Article v) => v.content;
  static const Field<Article, String> _f$content = Field('content', _$content);
  static int _$viewCounter(Article v) => v.viewCounter;
  static const Field<Article, int> _f$viewCounter =
      Field('viewCounter', _$viewCounter, opt: true, def: 0);
  static List<Tag> _$tags(Article v) => v.tags;
  static const Field<Article, List<Tag>> _f$tags =
      Field('tags', _$tags, opt: true, def: const []);
  static List<Attachment> _$attachments(Article v) => v.attachments;
  static const Field<Article, List<Attachment>> _f$attachments =
      Field('attachments', _$attachments, opt: true, def: const []);

  @override
  final Map<Symbol, Field<Article, dynamic>> fields = const {
    #id: _f$id,
    #title: _f$title,
    #content: _f$content,
    #viewCounter: _f$viewCounter,
    #tags: _f$tags,
    #attachments: _f$attachments,
  };

  static Article _instantiate(DecodingData data) {
    return Article(
        id: data.dec(_f$id),
        title: data.dec(_f$title),
        content: data.dec(_f$content),
        viewCounter: data.dec(_f$viewCounter),
        tags: data.dec(_f$tags),
        attachments: data.dec(_f$attachments));
  }

  @override
  final Function instantiate = _instantiate;

  static Article fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<Article>(map));
  }

  static Article fromJson(String json) {
    return _guard((c) => c.fromJson<Article>(json));
  }
}

mixin ArticleMappable {
  String toJson() {
    return ArticleMapper._guard((c) => c.toJson(this as Article));
  }

  Map<String, dynamic> toMap() {
    return ArticleMapper._guard((c) => c.toMap(this as Article));
  }

  ArticleCopyWith<Article, Article, Article> get copyWith =>
      _ArticleCopyWithImpl(this as Article, $identity, $identity);
  @override
  String toString() {
    return ArticleMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            ArticleMapper._guard((c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return ArticleMapper._guard((c) => c.hash(this));
  }
}

extension ArticleValueCopy<$R, $Out> on ObjectCopyWith<$R, Article, $Out> {
  ArticleCopyWith<$R, Article, $Out> get $asArticle =>
      $base.as((v, t, t2) => _ArticleCopyWithImpl(v, t, t2));
}

abstract class ArticleCopyWith<$R, $In extends Article, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  ListCopyWith<$R, Tag, TagCopyWith<$R, Tag, Tag>> get tags;
  ListCopyWith<$R, Attachment, AttachmentCopyWith<$R, Attachment, Attachment>>
      get attachments;
  $R call(
      {String? id,
      String? title,
      String? content,
      int? viewCounter,
      List<Tag>? tags,
      List<Attachment>? attachments});
  ArticleCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _ArticleCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, Article, $Out>
    implements ArticleCopyWith<$R, Article, $Out> {
  _ArticleCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<Article> $mapper =
      ArticleMapper.ensureInitialized();
  @override
  ListCopyWith<$R, Tag, TagCopyWith<$R, Tag, Tag>> get tags => ListCopyWith(
      $value.tags, (v, t) => v.copyWith.$chain(t), (v) => call(tags: v));
  @override
  ListCopyWith<$R, Attachment, AttachmentCopyWith<$R, Attachment, Attachment>>
      get attachments => ListCopyWith($value.attachments,
          (v, t) => v.copyWith.$chain(t), (v) => call(attachments: v));
  @override
  $R call(
          {String? id,
          String? title,
          String? content,
          int? viewCounter,
          List<Tag>? tags,
          List<Attachment>? attachments}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (content != null) #content: content,
        if (viewCounter != null) #viewCounter: viewCounter,
        if (tags != null) #tags: tags,
        if (attachments != null) #attachments: attachments
      }));
  @override
  Article $make(CopyWithData data) => Article(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      content: data.get(#content, or: $value.content),
      viewCounter: data.get(#viewCounter, or: $value.viewCounter),
      tags: data.get(#tags, or: $value.tags),
      attachments: data.get(#attachments, or: $value.attachments));

  @override
  ArticleCopyWith<$R2, Article, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
      _ArticleCopyWithImpl($value, $cast, t);
}
