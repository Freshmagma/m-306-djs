// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'attachment.dart';

class AttachmentMapper extends ClassMapperBase<Attachment> {
  AttachmentMapper._();

  static AttachmentMapper? _instance;
  static AttachmentMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = AttachmentMapper._());
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'Attachment';

  static String _$id(Attachment v) => v.id;
  static const Field<Attachment, String> _f$id = Field('id', _$id);
  static String _$title(Attachment v) => v.title;
  static const Field<Attachment, String> _f$title = Field('title', _$title);
  static String _$url(Attachment v) => v.url;
  static const Field<Attachment, String> _f$url = Field('url', _$url);

  @override
  final Map<Symbol, Field<Attachment, dynamic>> fields = const {
    #id: _f$id,
    #title: _f$title,
    #url: _f$url,
  };

  static Attachment _instantiate(DecodingData data) {
    throw MapperException.missingConstructor('Attachment');
  }

  @override
  final Function instantiate = _instantiate;

  static Attachment fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<Attachment>(map));
  }

  static Attachment fromJson(String json) {
    return _guard((c) => c.fromJson<Attachment>(json));
  }
}

mixin AttachmentMappable {
  String toJson();
  Map<String, dynamic> toMap();
  AttachmentCopyWith<Attachment, Attachment, Attachment> get copyWith;
}

abstract class AttachmentCopyWith<$R, $In extends Attachment, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? id, String? title, String? url});
  AttachmentCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}
