// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'external_link.dart';

class ExternalLinkMapper extends SubClassMapperBase<ExternalLink> {
  ExternalLinkMapper._();

  static ExternalLinkMapper? _instance;
  static ExternalLinkMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = ExternalLinkMapper._());
      AttachmentMapper.ensureInitialized().addSubMapper(_instance!);
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'ExternalLink';

  static String _$id(ExternalLink v) => v.id;
  static const Field<ExternalLink, dynamic> _f$id = Field('id', _$id);
  static String _$title(ExternalLink v) => v.title;
  static const Field<ExternalLink, dynamic> _f$title = Field('title', _$title);
  static String _$url(ExternalLink v) => v.url;
  static const Field<ExternalLink, dynamic> _f$url = Field('url', _$url);

  @override
  final Map<Symbol, Field<ExternalLink, dynamic>> fields = const {
    #id: _f$id,
    #title: _f$title,
    #url: _f$url,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = "link";
  @override
  late final ClassMapperBase superMapper = AttachmentMapper.ensureInitialized();

  static ExternalLink _instantiate(DecodingData data) {
    return ExternalLink(
        id: data.dec(_f$id), title: data.dec(_f$title), url: data.dec(_f$url));
  }

  @override
  final Function instantiate = _instantiate;

  static ExternalLink fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<ExternalLink>(map));
  }

  static ExternalLink fromJson(String json) {
    return _guard((c) => c.fromJson<ExternalLink>(json));
  }
}

mixin ExternalLinkMappable {
  String toJson() {
    return ExternalLinkMapper._guard((c) => c.toJson(this as ExternalLink));
  }

  Map<String, dynamic> toMap() {
    return ExternalLinkMapper._guard((c) => c.toMap(this as ExternalLink));
  }

  ExternalLinkCopyWith<ExternalLink, ExternalLink, ExternalLink> get copyWith =>
      _ExternalLinkCopyWithImpl(this as ExternalLink, $identity, $identity);
  @override
  String toString() {
    return ExternalLinkMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            ExternalLinkMapper._guard((c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return ExternalLinkMapper._guard((c) => c.hash(this));
  }
}

extension ExternalLinkValueCopy<$R, $Out>
    on ObjectCopyWith<$R, ExternalLink, $Out> {
  ExternalLinkCopyWith<$R, ExternalLink, $Out> get $asExternalLink =>
      $base.as((v, t, t2) => _ExternalLinkCopyWithImpl(v, t, t2));
}

abstract class ExternalLinkCopyWith<$R, $In extends ExternalLink, $Out>
    implements AttachmentCopyWith<$R, $In, $Out> {
  @override
  $R call({dynamic id, dynamic title, dynamic url});
  ExternalLinkCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _ExternalLinkCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, ExternalLink, $Out>
    implements ExternalLinkCopyWith<$R, ExternalLink, $Out> {
  _ExternalLinkCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<ExternalLink> $mapper =
      ExternalLinkMapper.ensureInitialized();
  @override
  $R call({Object? id = $none, Object? title = $none, Object? url = $none}) =>
      $apply(FieldCopyWithData({
        if (id != $none) #id: id,
        if (title != $none) #title: title,
        if (url != $none) #url: url
      }));
  @override
  ExternalLink $make(CopyWithData data) => ExternalLink(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      url: data.get(#url, or: $value.url));

  @override
  ExternalLinkCopyWith<$R2, ExternalLink, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _ExternalLinkCopyWithImpl($value, $cast, t);
}
