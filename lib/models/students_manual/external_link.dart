import 'package:dart_mappable/dart_mappable.dart';
import 'attachment.dart';

part 'external_link.mapper.dart';

@MappableClass(discriminatorValue: "link")
class ExternalLink extends Attachment with ExternalLinkMappable {
  const ExternalLink({required id, required title, required url})
      : super(id: id, title: title, url: url);

  static const fromMap = ExternalLinkMapper.fromMap;
  static const fromJson = ExternalLinkMapper.fromJson;
}
