// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'stage_activity_answer.dart';

class StageActivityAnswerMapper extends ClassMapperBase<StageActivityAnswer> {
  StageActivityAnswerMapper._();

  static StageActivityAnswerMapper? _instance;
  static StageActivityAnswerMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = StageActivityAnswerMapper._());
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'StageActivityAnswer';

  static String _$id(StageActivityAnswer v) => v.id;
  static const Field<StageActivityAnswer, String> _f$id = Field('id', _$id);
  static String _$answerText(StageActivityAnswer v) => v.answerText;
  static const Field<StageActivityAnswer, String> _f$answerText =
      Field('answerText', _$answerText);

  @override
  final Map<Symbol, Field<StageActivityAnswer, dynamic>> fields = const {
    #id: _f$id,
    #answerText: _f$answerText,
  };

  static StageActivityAnswer _instantiate(DecodingData data) {
    return StageActivityAnswer(
        id: data.dec(_f$id), answerText: data.dec(_f$answerText));
  }

  @override
  final Function instantiate = _instantiate;

  static StageActivityAnswer fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<StageActivityAnswer>(map));
  }

  static StageActivityAnswer fromJson(String json) {
    return _guard((c) => c.fromJson<StageActivityAnswer>(json));
  }
}

mixin StageActivityAnswerMappable {
  String toJson() {
    return StageActivityAnswerMapper._guard(
        (c) => c.toJson(this as StageActivityAnswer));
  }

  Map<String, dynamic> toMap() {
    return StageActivityAnswerMapper._guard(
        (c) => c.toMap(this as StageActivityAnswer));
  }

  StageActivityAnswerCopyWith<StageActivityAnswer, StageActivityAnswer,
          StageActivityAnswer>
      get copyWith => _StageActivityAnswerCopyWithImpl(
          this as StageActivityAnswer, $identity, $identity);
  @override
  String toString() {
    return StageActivityAnswerMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            StageActivityAnswerMapper._guard((c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return StageActivityAnswerMapper._guard((c) => c.hash(this));
  }
}

extension StageActivityAnswerValueCopy<$R, $Out>
    on ObjectCopyWith<$R, StageActivityAnswer, $Out> {
  StageActivityAnswerCopyWith<$R, StageActivityAnswer, $Out>
      get $asStageActivityAnswer =>
          $base.as((v, t, t2) => _StageActivityAnswerCopyWithImpl(v, t, t2));
}

abstract class StageActivityAnswerCopyWith<$R, $In extends StageActivityAnswer,
    $Out> implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? id, String? answerText});
  StageActivityAnswerCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _StageActivityAnswerCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, StageActivityAnswer, $Out>
    implements StageActivityAnswerCopyWith<$R, StageActivityAnswer, $Out> {
  _StageActivityAnswerCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<StageActivityAnswer> $mapper =
      StageActivityAnswerMapper.ensureInitialized();
  @override
  $R call({String? id, String? answerText}) => $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (answerText != null) #answerText: answerText
      }));
  @override
  StageActivityAnswer $make(CopyWithData data) => StageActivityAnswer(
      id: data.get(#id, or: $value.id),
      answerText: data.get(#answerText, or: $value.answerText));

  @override
  StageActivityAnswerCopyWith<$R2, StageActivityAnswer, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _StageActivityAnswerCopyWithImpl($value, $cast, t);
}
