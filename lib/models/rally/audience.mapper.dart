// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'audience.dart';

class AudienceMapper extends ClassMapperBase<Audience> {
  AudienceMapper._();

  static AudienceMapper? _instance;
  static AudienceMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = AudienceMapper._());
      ParticipatingPartyMapper.ensureInitialized();
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'Audience';

  static String _$id(Audience v) => v.id;
  static const Field<Audience, String> _f$id = Field('id', _$id);
  static String _$title(Audience v) => v.title;
  static const Field<Audience, String> _f$title = Field('title', _$title);
  static List<ParticipatingParty> _$participatingParties(Audience v) =>
      v.participatingParties;
  static const Field<Audience, List<ParticipatingParty>>
      _f$participatingParties =
      Field('participatingParties', _$participatingParties);

  @override
  final Map<Symbol, Field<Audience, dynamic>> fields = const {
    #id: _f$id,
    #title: _f$title,
    #participatingParties: _f$participatingParties,
  };

  static Audience _instantiate(DecodingData data) {
    return Audience(
        id: data.dec(_f$id),
        title: data.dec(_f$title),
        participatingParties: data.dec(_f$participatingParties));
  }

  @override
  final Function instantiate = _instantiate;

  static Audience fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<Audience>(map));
  }

  static Audience fromJson(String json) {
    return _guard((c) => c.fromJson<Audience>(json));
  }
}

mixin AudienceMappable {
  String toJson() {
    return AudienceMapper._guard((c) => c.toJson(this as Audience));
  }

  Map<String, dynamic> toMap() {
    return AudienceMapper._guard((c) => c.toMap(this as Audience));
  }

  AudienceCopyWith<Audience, Audience, Audience> get copyWith =>
      _AudienceCopyWithImpl(this as Audience, $identity, $identity);
  @override
  String toString() {
    return AudienceMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            AudienceMapper._guard((c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return AudienceMapper._guard((c) => c.hash(this));
  }
}

extension AudienceValueCopy<$R, $Out> on ObjectCopyWith<$R, Audience, $Out> {
  AudienceCopyWith<$R, Audience, $Out> get $asAudience =>
      $base.as((v, t, t2) => _AudienceCopyWithImpl(v, t, t2));
}

abstract class AudienceCopyWith<$R, $In extends Audience, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  ListCopyWith<
      $R,
      ParticipatingParty,
      ParticipatingPartyCopyWith<$R, ParticipatingParty,
          ParticipatingParty>> get participatingParties;
  $R call(
      {String? id,
      String? title,
      List<ParticipatingParty>? participatingParties});
  AudienceCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _AudienceCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, Audience, $Out>
    implements AudienceCopyWith<$R, Audience, $Out> {
  _AudienceCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<Audience> $mapper =
      AudienceMapper.ensureInitialized();
  @override
  ListCopyWith<
      $R,
      ParticipatingParty,
      ParticipatingPartyCopyWith<$R, ParticipatingParty,
          ParticipatingParty>> get participatingParties => ListCopyWith(
      $value.participatingParties,
      (v, t) => v.copyWith.$chain(t),
      (v) => call(participatingParties: v));
  @override
  $R call(
          {String? id,
          String? title,
          List<ParticipatingParty>? participatingParties}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (participatingParties != null)
          #participatingParties: participatingParties
      }));
  @override
  Audience $make(CopyWithData data) => Audience(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      participatingParties:
          data.get(#participatingParties, or: $value.participatingParties));

  @override
  AudienceCopyWith<$R2, Audience, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _AudienceCopyWithImpl($value, $cast, t);
}
