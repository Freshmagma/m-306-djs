// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'participating_party_update_request_dto.dart';

class ParticipatingPartyUpdateRequestDtoMapper
    extends ClassMapperBase<ParticipatingPartyUpdateRequestDto> {
  ParticipatingPartyUpdateRequestDtoMapper._();

  static ParticipatingPartyUpdateRequestDtoMapper? _instance;
  static ParticipatingPartyUpdateRequestDtoMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = ParticipatingPartyUpdateRequestDtoMapper._());
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'ParticipatingPartyUpdateRequestDto';

  static String _$id(ParticipatingPartyUpdateRequestDto v) => v.id;
  static const Field<ParticipatingPartyUpdateRequestDto, String> _f$id =
      Field('id', _$id);
  static String _$title(ParticipatingPartyUpdateRequestDto v) => v.title;
  static const Field<ParticipatingPartyUpdateRequestDto, String> _f$title =
      Field('title', _$title);
  static List<String> _$names(ParticipatingPartyUpdateRequestDto v) => v.names;
  static const Field<ParticipatingPartyUpdateRequestDto, List<String>>
      _f$names = Field('names', _$names);

  @override
  final Map<Symbol, Field<ParticipatingPartyUpdateRequestDto, dynamic>> fields =
      const {
    #id: _f$id,
    #title: _f$title,
    #names: _f$names,
  };

  static ParticipatingPartyUpdateRequestDto _instantiate(DecodingData data) {
    return ParticipatingPartyUpdateRequestDto(
        id: data.dec(_f$id),
        title: data.dec(_f$title),
        names: data.dec(_f$names));
  }

  @override
  final Function instantiate = _instantiate;

  static ParticipatingPartyUpdateRequestDto fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<ParticipatingPartyUpdateRequestDto>(map));
  }

  static ParticipatingPartyUpdateRequestDto fromJson(String json) {
    return _guard((c) => c.fromJson<ParticipatingPartyUpdateRequestDto>(json));
  }
}

mixin ParticipatingPartyUpdateRequestDtoMappable {
  String toJson() {
    return ParticipatingPartyUpdateRequestDtoMapper._guard(
        (c) => c.toJson(this as ParticipatingPartyUpdateRequestDto));
  }

  Map<String, dynamic> toMap() {
    return ParticipatingPartyUpdateRequestDtoMapper._guard(
        (c) => c.toMap(this as ParticipatingPartyUpdateRequestDto));
  }

  ParticipatingPartyUpdateRequestDtoCopyWith<
          ParticipatingPartyUpdateRequestDto,
          ParticipatingPartyUpdateRequestDto,
          ParticipatingPartyUpdateRequestDto>
      get copyWith => _ParticipatingPartyUpdateRequestDtoCopyWithImpl(
          this as ParticipatingPartyUpdateRequestDto, $identity, $identity);
  @override
  String toString() {
    return ParticipatingPartyUpdateRequestDtoMapper._guard(
        (c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            ParticipatingPartyUpdateRequestDtoMapper._guard(
                (c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return ParticipatingPartyUpdateRequestDtoMapper._guard((c) => c.hash(this));
  }
}

extension ParticipatingPartyUpdateRequestDtoValueCopy<$R, $Out>
    on ObjectCopyWith<$R, ParticipatingPartyUpdateRequestDto, $Out> {
  ParticipatingPartyUpdateRequestDtoCopyWith<$R,
          ParticipatingPartyUpdateRequestDto, $Out>
      get $asParticipatingPartyUpdateRequestDto => $base.as((v, t, t2) =>
          _ParticipatingPartyUpdateRequestDtoCopyWithImpl(v, t, t2));
}

abstract class ParticipatingPartyUpdateRequestDtoCopyWith<
    $R,
    $In extends ParticipatingPartyUpdateRequestDto,
    $Out> implements ClassCopyWith<$R, $In, $Out> {
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>> get names;
  $R call({String? id, String? title, List<String>? names});
  ParticipatingPartyUpdateRequestDtoCopyWith<$R2, $In, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _ParticipatingPartyUpdateRequestDtoCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, ParticipatingPartyUpdateRequestDto, $Out>
    implements
        ParticipatingPartyUpdateRequestDtoCopyWith<$R,
            ParticipatingPartyUpdateRequestDto, $Out> {
  _ParticipatingPartyUpdateRequestDtoCopyWithImpl(
      super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<ParticipatingPartyUpdateRequestDto> $mapper =
      ParticipatingPartyUpdateRequestDtoMapper.ensureInitialized();
  @override
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>> get names =>
      ListCopyWith($value.names, (v, t) => ObjectCopyWith(v, $identity, t),
          (v) => call(names: v));
  @override
  $R call({String? id, String? title, List<String>? names}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (names != null) #names: names
      }));
  @override
  ParticipatingPartyUpdateRequestDto $make(CopyWithData data) =>
      ParticipatingPartyUpdateRequestDto(
          id: data.get(#id, or: $value.id),
          title: data.get(#title, or: $value.title),
          names: data.get(#names, or: $value.names));

  @override
  ParticipatingPartyUpdateRequestDtoCopyWith<$R2,
      ParticipatingPartyUpdateRequestDto, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _ParticipatingPartyUpdateRequestDtoCopyWithImpl($value, $cast, t);
}
