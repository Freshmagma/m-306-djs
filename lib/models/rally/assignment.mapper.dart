// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'assignment.dart';

class AssignmentMapper extends ClassMapperBase<Assignment> {
  AssignmentMapper._();

  static AssignmentMapper? _instance;
  static AssignmentMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = AssignmentMapper._());
      AudienceMapper.ensureInitialized();
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'Assignment';

  static String _$id(Assignment v) => v.id;
  static const Field<Assignment, String> _f$id = Field('id', _$id);
  static int _$joiningCode(Assignment v) => v.joiningCode;
  static const Field<Assignment, int> _f$joiningCode =
      Field('joiningCode', _$joiningCode);
  static Audience _$audience(Assignment v) => v.audience;
  static const Field<Assignment, Audience> _f$audience =
      Field('audience', _$audience);

  @override
  final Map<Symbol, Field<Assignment, dynamic>> fields = const {
    #id: _f$id,
    #joiningCode: _f$joiningCode,
    #audience: _f$audience,
  };

  static Assignment _instantiate(DecodingData data) {
    return Assignment(
        id: data.dec(_f$id),
        joiningCode: data.dec(_f$joiningCode),
        audience: data.dec(_f$audience));
  }

  @override
  final Function instantiate = _instantiate;

  static Assignment fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<Assignment>(map));
  }

  static Assignment fromJson(String json) {
    return _guard((c) => c.fromJson<Assignment>(json));
  }
}

mixin AssignmentMappable {
  String toJson() {
    return AssignmentMapper._guard((c) => c.toJson(this as Assignment));
  }

  Map<String, dynamic> toMap() {
    return AssignmentMapper._guard((c) => c.toMap(this as Assignment));
  }

  AssignmentCopyWith<Assignment, Assignment, Assignment> get copyWith =>
      _AssignmentCopyWithImpl(this as Assignment, $identity, $identity);
  @override
  String toString() {
    return AssignmentMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            AssignmentMapper._guard((c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return AssignmentMapper._guard((c) => c.hash(this));
  }
}

extension AssignmentValueCopy<$R, $Out>
    on ObjectCopyWith<$R, Assignment, $Out> {
  AssignmentCopyWith<$R, Assignment, $Out> get $asAssignment =>
      $base.as((v, t, t2) => _AssignmentCopyWithImpl(v, t, t2));
}

abstract class AssignmentCopyWith<$R, $In extends Assignment, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  AudienceCopyWith<$R, Audience, Audience> get audience;
  $R call({String? id, int? joiningCode, Audience? audience});
  AssignmentCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _AssignmentCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, Assignment, $Out>
    implements AssignmentCopyWith<$R, Assignment, $Out> {
  _AssignmentCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<Assignment> $mapper =
      AssignmentMapper.ensureInitialized();
  @override
  AudienceCopyWith<$R, Audience, Audience> get audience =>
      $value.audience.copyWith.$chain((v) => call(audience: v));
  @override
  $R call({String? id, int? joiningCode, Audience? audience}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (joiningCode != null) #joiningCode: joiningCode,
        if (audience != null) #audience: audience
      }));
  @override
  Assignment $make(CopyWithData data) => Assignment(
      id: data.get(#id, or: $value.id),
      joiningCode: data.get(#joiningCode, or: $value.joiningCode),
      audience: data.get(#audience, or: $value.audience));

  @override
  AssignmentCopyWith<$R2, Assignment, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _AssignmentCopyWithImpl($value, $cast, t);
}
