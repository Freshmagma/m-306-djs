import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';
import 'package:gibz_mobileapp/models/rally/stage_activity_answer.dart';

part 'participating_party_answer.mapper.dart';

@MappableClass(discriminatorKey: "type")
abstract class ParticipatingPartyAnswer extends Equatable
    with ParticipatingPartyAnswerMappable {
  final String id;
  final bool isCorrect;

  const ParticipatingPartyAnswer({
    required this.id,
    required this.isCorrect,
  });

  @override
  List<Object?> get props => [id];
}

@MappableClass(discriminatorValue: "closedQuestionAnswer")
class ClosedQuestionAnswer extends ParticipatingPartyAnswer
    with ClosedQuestionAnswerMappable {
  final StageActivityAnswer answer;

  const ClosedQuestionAnswer({
    required String id,
    required bool isCorrect,
    required this.answer,
  }) : super(id: id, isCorrect: isCorrect);
}

@MappableClass(discriminatorValue: "openQuestionAnswer")
class OpenQuestionAnswer extends ParticipatingPartyAnswer
    with OpenQuestionAnswerMappable {
  final String answerText;

  const OpenQuestionAnswer({
    required String id,
    required bool isCorrect,
    required this.answerText,
  }) : super(id: id, isCorrect: isCorrect);
}
