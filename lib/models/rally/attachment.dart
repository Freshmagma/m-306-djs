import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

part 'attachment.mapper.dart';

@MappableClass(discriminatorKey: "type")
abstract class Attachment extends Equatable with AttachmentMappable {
  final String id;
  final String title;
  final String url;

  Attachment({
    required this.id,
    required this.title,
    required this.url,
  });

  @override
  List<Object?> get props => [id];
}

@MappableClass(discriminatorValue: "video")
class VideoAttachment extends Attachment with VideoAttachmentMappable {
  VideoAttachment({
    required String id,
    required String title,
    required String url,
  }) : super(id: id, title: title, url: url);
}

@MappableClass(discriminatorValue: "link")
class ExternalLinkAttachment extends Attachment
    with ExternalLinkAttachmentMappable {
  ExternalLinkAttachment({
    required String id,
    required String title,
    required String url,
  }) : super(id: id, title: title, url: url);
}
