// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'location_marker.dart';

class LocationMarkerMapper extends ClassMapperBase<LocationMarker> {
  LocationMarkerMapper._();

  static LocationMarkerMapper? _instance;
  static LocationMarkerMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = LocationMarkerMapper._());
      IBeaconLocationMarkerMapper.ensureInitialized();
      QRCodeLocationMarkerMapper.ensureInitialized();
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'LocationMarker';

  static String _$id(LocationMarker v) => v.id;
  static const Field<LocationMarker, String> _f$id = Field('id', _$id);

  @override
  final Map<Symbol, Field<LocationMarker, dynamic>> fields = const {
    #id: _f$id,
  };

  static LocationMarker _instantiate(DecodingData data) {
    throw MapperException.missingSubclass(
        'LocationMarker', 'type', '${data.value['type']}');
  }

  @override
  final Function instantiate = _instantiate;

  static LocationMarker fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<LocationMarker>(map));
  }

  static LocationMarker fromJson(String json) {
    return _guard((c) => c.fromJson<LocationMarker>(json));
  }
}

mixin LocationMarkerMappable {
  String toJson();
  Map<String, dynamic> toMap();
  LocationMarkerCopyWith<LocationMarker, LocationMarker, LocationMarker>
      get copyWith;
}

abstract class LocationMarkerCopyWith<$R, $In extends LocationMarker, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? id});
  LocationMarkerCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class IBeaconLocationMarkerMapper
    extends SubClassMapperBase<IBeaconLocationMarker> {
  IBeaconLocationMarkerMapper._();

  static IBeaconLocationMarkerMapper? _instance;
  static IBeaconLocationMarkerMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = IBeaconLocationMarkerMapper._());
      LocationMarkerMapper.ensureInitialized().addSubMapper(_instance!);
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'IBeaconLocationMarker';

  static String _$id(IBeaconLocationMarker v) => v.id;
  static const Field<IBeaconLocationMarker, String> _f$id = Field('id', _$id);
  static String _$uuid(IBeaconLocationMarker v) => v.uuid;
  static const Field<IBeaconLocationMarker, String> _f$uuid =
      Field('uuid', _$uuid);
  static int? _$major(IBeaconLocationMarker v) => v.major;
  static const Field<IBeaconLocationMarker, int> _f$major =
      Field('major', _$major);
  static int? _$minor(IBeaconLocationMarker v) => v.minor;
  static const Field<IBeaconLocationMarker, int> _f$minor =
      Field('minor', _$minor);

  @override
  final Map<Symbol, Field<IBeaconLocationMarker, dynamic>> fields = const {
    #id: _f$id,
    #uuid: _f$uuid,
    #major: _f$major,
    #minor: _f$minor,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = "IBeaconLocationMarker";
  @override
  late final ClassMapperBase superMapper =
      LocationMarkerMapper.ensureInitialized();

  static IBeaconLocationMarker _instantiate(DecodingData data) {
    return IBeaconLocationMarker(data.dec(_f$id),
        uuid: data.dec(_f$uuid),
        major: data.dec(_f$major),
        minor: data.dec(_f$minor));
  }

  @override
  final Function instantiate = _instantiate;

  static IBeaconLocationMarker fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<IBeaconLocationMarker>(map));
  }

  static IBeaconLocationMarker fromJson(String json) {
    return _guard((c) => c.fromJson<IBeaconLocationMarker>(json));
  }
}

mixin IBeaconLocationMarkerMappable {
  String toJson() {
    return IBeaconLocationMarkerMapper._guard(
        (c) => c.toJson(this as IBeaconLocationMarker));
  }

  Map<String, dynamic> toMap() {
    return IBeaconLocationMarkerMapper._guard(
        (c) => c.toMap(this as IBeaconLocationMarker));
  }

  IBeaconLocationMarkerCopyWith<IBeaconLocationMarker, IBeaconLocationMarker,
          IBeaconLocationMarker>
      get copyWith => _IBeaconLocationMarkerCopyWithImpl(
          this as IBeaconLocationMarker, $identity, $identity);
  @override
  String toString() {
    return IBeaconLocationMarkerMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            IBeaconLocationMarkerMapper._guard((c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return IBeaconLocationMarkerMapper._guard((c) => c.hash(this));
  }
}

extension IBeaconLocationMarkerValueCopy<$R, $Out>
    on ObjectCopyWith<$R, IBeaconLocationMarker, $Out> {
  IBeaconLocationMarkerCopyWith<$R, IBeaconLocationMarker, $Out>
      get $asIBeaconLocationMarker =>
          $base.as((v, t, t2) => _IBeaconLocationMarkerCopyWithImpl(v, t, t2));
}

abstract class IBeaconLocationMarkerCopyWith<
    $R,
    $In extends IBeaconLocationMarker,
    $Out> implements LocationMarkerCopyWith<$R, $In, $Out> {
  @override
  $R call({String? id, String? uuid, int? major, int? minor});
  IBeaconLocationMarkerCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _IBeaconLocationMarkerCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, IBeaconLocationMarker, $Out>
    implements IBeaconLocationMarkerCopyWith<$R, IBeaconLocationMarker, $Out> {
  _IBeaconLocationMarkerCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<IBeaconLocationMarker> $mapper =
      IBeaconLocationMarkerMapper.ensureInitialized();
  @override
  $R call(
          {String? id,
          String? uuid,
          Object? major = $none,
          Object? minor = $none}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (uuid != null) #uuid: uuid,
        if (major != $none) #major: major,
        if (minor != $none) #minor: minor
      }));
  @override
  IBeaconLocationMarker $make(CopyWithData data) =>
      IBeaconLocationMarker(data.get(#id, or: $value.id),
          uuid: data.get(#uuid, or: $value.uuid),
          major: data.get(#major, or: $value.major),
          minor: data.get(#minor, or: $value.minor));

  @override
  IBeaconLocationMarkerCopyWith<$R2, IBeaconLocationMarker, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _IBeaconLocationMarkerCopyWithImpl($value, $cast, t);
}

class QRCodeLocationMarkerMapper
    extends SubClassMapperBase<QRCodeLocationMarker> {
  QRCodeLocationMarkerMapper._();

  static QRCodeLocationMarkerMapper? _instance;
  static QRCodeLocationMarkerMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = QRCodeLocationMarkerMapper._());
      LocationMarkerMapper.ensureInitialized().addSubMapper(_instance!);
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'QRCodeLocationMarker';

  static String _$id(QRCodeLocationMarker v) => v.id;
  static const Field<QRCodeLocationMarker, String> _f$id = Field('id', _$id);
  static String _$content(QRCodeLocationMarker v) => v.content;
  static const Field<QRCodeLocationMarker, String> _f$content =
      Field('content', _$content);

  @override
  final Map<Symbol, Field<QRCodeLocationMarker, dynamic>> fields = const {
    #id: _f$id,
    #content: _f$content,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = "QRCodeLocationMarker";
  @override
  late final ClassMapperBase superMapper =
      LocationMarkerMapper.ensureInitialized();

  static QRCodeLocationMarker _instantiate(DecodingData data) {
    return QRCodeLocationMarker(data.dec(_f$id), content: data.dec(_f$content));
  }

  @override
  final Function instantiate = _instantiate;

  static QRCodeLocationMarker fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<QRCodeLocationMarker>(map));
  }

  static QRCodeLocationMarker fromJson(String json) {
    return _guard((c) => c.fromJson<QRCodeLocationMarker>(json));
  }
}

mixin QRCodeLocationMarkerMappable {
  String toJson() {
    return QRCodeLocationMarkerMapper._guard(
        (c) => c.toJson(this as QRCodeLocationMarker));
  }

  Map<String, dynamic> toMap() {
    return QRCodeLocationMarkerMapper._guard(
        (c) => c.toMap(this as QRCodeLocationMarker));
  }

  QRCodeLocationMarkerCopyWith<QRCodeLocationMarker, QRCodeLocationMarker,
          QRCodeLocationMarker>
      get copyWith => _QRCodeLocationMarkerCopyWithImpl(
          this as QRCodeLocationMarker, $identity, $identity);
  @override
  String toString() {
    return QRCodeLocationMarkerMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            QRCodeLocationMarkerMapper._guard((c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return QRCodeLocationMarkerMapper._guard((c) => c.hash(this));
  }
}

extension QRCodeLocationMarkerValueCopy<$R, $Out>
    on ObjectCopyWith<$R, QRCodeLocationMarker, $Out> {
  QRCodeLocationMarkerCopyWith<$R, QRCodeLocationMarker, $Out>
      get $asQRCodeLocationMarker =>
          $base.as((v, t, t2) => _QRCodeLocationMarkerCopyWithImpl(v, t, t2));
}

abstract class QRCodeLocationMarkerCopyWith<
    $R,
    $In extends QRCodeLocationMarker,
    $Out> implements LocationMarkerCopyWith<$R, $In, $Out> {
  @override
  $R call({String? id, String? content});
  QRCodeLocationMarkerCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _QRCodeLocationMarkerCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, QRCodeLocationMarker, $Out>
    implements QRCodeLocationMarkerCopyWith<$R, QRCodeLocationMarker, $Out> {
  _QRCodeLocationMarkerCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<QRCodeLocationMarker> $mapper =
      QRCodeLocationMarkerMapper.ensureInitialized();
  @override
  $R call({String? id, String? content}) => $apply(FieldCopyWithData(
      {if (id != null) #id: id, if (content != null) #content: content}));
  @override
  QRCodeLocationMarker $make(CopyWithData data) =>
      QRCodeLocationMarker(data.get(#id, or: $value.id),
          content: data.get(#content, or: $value.content));

  @override
  QRCodeLocationMarkerCopyWith<$R2, QRCodeLocationMarker, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _QRCodeLocationMarkerCopyWithImpl($value, $cast, t);
}
