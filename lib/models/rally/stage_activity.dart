import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';
import 'stage_activity_answer.dart';

part 'stage_activity.mapper.dart';

@MappableClass(discriminatorKey: "type")
abstract class StageActivity extends Equatable with StageActivityMappable {
  final String id;
  final String title;
  final String task;
  final int maxPoints;

  const StageActivity({
    required this.id,
    required this.title,
    required this.task,
    required this.maxPoints,
  });

  @override
  List<Object?> get props => [id];
}

@MappableClass(discriminatorKey: "type")
abstract class ClosedQuestionActivity extends StageActivity
    with ClosedQuestionActivityMappable {
  final List<StageActivityAnswer> answers;

  const ClosedQuestionActivity({
    required String id,
    required String title,
    required String task,
    required int maxPoints,
    required this.answers,
  }) : super(id: id, title: title, task: task, maxPoints: maxPoints);
}

@MappableClass(discriminatorValue: "singleChoice")
class SingleChoiceActivity extends ClosedQuestionActivity
    with SingleChoiceActivityMappable {
  const SingleChoiceActivity({
    required String id,
    required String title,
    required String task,
    required int maxPoints,
    required List<StageActivityAnswer> answers,
  }) : super(
          id: id,
          title: title,
          task: task,
          maxPoints: maxPoints,
          answers: answers,
        );
}

@MappableClass(discriminatorValue: "multipleChoice")
class MultipleChoiceActivity extends ClosedQuestionActivity
    with MultipleChoiceActivityMappable {
  const MultipleChoiceActivity({
    required String id,
    required String title,
    required String task,
    required int maxPoints,
    required List<StageActivityAnswer> answers,
  }) : super(
          id: id,
          title: title,
          task: task,
          maxPoints: maxPoints,
          answers: answers,
        );
}

@MappableClass(discriminatorValue: "textInput")
class TextInputActivity extends StageActivity with TextInputActivityMappable {
  const TextInputActivity(
      {required String id,
      required String title,
      required String task,
      required int maxPoints})
      : super(id: id, title: title, task: task, maxPoints: maxPoints);
}
