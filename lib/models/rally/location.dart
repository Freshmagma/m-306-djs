import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';
import 'location_marker.dart';

part 'location.mapper.dart';

@MappableClass()
class Location extends Equatable with LocationMappable {
  final String id;
  final String title;
  final List<LocationMarker> locationMarkers;

  Location({
    required this.id,
    required this.title,
    required this.locationMarkers,
  });

  @override
  List<Object?> get props => [id];
}
