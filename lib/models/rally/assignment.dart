import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';
import 'audience.dart';

part 'assignment.mapper.dart';

@MappableClass()
class Assignment extends Equatable with AssignmentMappable {
  final String id;
  final int joiningCode;
  final Audience audience;

  Assignment({
    required this.id,
    required this.joiningCode,
    required this.audience,
  });

  @override
  List<Object?> get props => [id, joiningCode];
}
