// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'participating_party_appearance.dart';

class ParticipatingPartyAppearanceMapper
    extends ClassMapperBase<ParticipatingPartyAppearance> {
  ParticipatingPartyAppearanceMapper._();

  static ParticipatingPartyAppearanceMapper? _instance;
  static ParticipatingPartyAppearanceMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = ParticipatingPartyAppearanceMapper._());
      ParticipatingPartyMapper.ensureInitialized();
      LocationMarkerMapper.ensureInitialized();
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'ParticipatingPartyAppearance';

  static String _$id(ParticipatingPartyAppearance v) => v.id;
  static const Field<ParticipatingPartyAppearance, String> _f$id =
      Field('id', _$id);
  static ParticipatingParty _$participatingParty(
          ParticipatingPartyAppearance v) =>
      v.participatingParty;
  static const Field<ParticipatingPartyAppearance, ParticipatingParty>
      _f$participatingParty = Field('participatingParty', _$participatingParty);
  static LocationMarker _$locationMarker(ParticipatingPartyAppearance v) =>
      v.locationMarker;
  static const Field<ParticipatingPartyAppearance, LocationMarker>
      _f$locationMarker = Field('locationMarker', _$locationMarker);

  @override
  final Map<Symbol, Field<ParticipatingPartyAppearance, dynamic>> fields =
      const {
    #id: _f$id,
    #participatingParty: _f$participatingParty,
    #locationMarker: _f$locationMarker,
  };

  static ParticipatingPartyAppearance _instantiate(DecodingData data) {
    return ParticipatingPartyAppearance(
        id: data.dec(_f$id),
        participatingParty: data.dec(_f$participatingParty),
        locationMarker: data.dec(_f$locationMarker));
  }

  @override
  final Function instantiate = _instantiate;

  static ParticipatingPartyAppearance fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<ParticipatingPartyAppearance>(map));
  }

  static ParticipatingPartyAppearance fromJson(String json) {
    return _guard((c) => c.fromJson<ParticipatingPartyAppearance>(json));
  }
}

mixin ParticipatingPartyAppearanceMappable {
  String toJson() {
    return ParticipatingPartyAppearanceMapper._guard(
        (c) => c.toJson(this as ParticipatingPartyAppearance));
  }

  Map<String, dynamic> toMap() {
    return ParticipatingPartyAppearanceMapper._guard(
        (c) => c.toMap(this as ParticipatingPartyAppearance));
  }

  ParticipatingPartyAppearanceCopyWith<ParticipatingPartyAppearance,
          ParticipatingPartyAppearance, ParticipatingPartyAppearance>
      get copyWith => _ParticipatingPartyAppearanceCopyWithImpl(
          this as ParticipatingPartyAppearance, $identity, $identity);
  @override
  String toString() {
    return ParticipatingPartyAppearanceMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            ParticipatingPartyAppearanceMapper._guard(
                (c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return ParticipatingPartyAppearanceMapper._guard((c) => c.hash(this));
  }
}

extension ParticipatingPartyAppearanceValueCopy<$R, $Out>
    on ObjectCopyWith<$R, ParticipatingPartyAppearance, $Out> {
  ParticipatingPartyAppearanceCopyWith<$R, ParticipatingPartyAppearance, $Out>
      get $asParticipatingPartyAppearance => $base.as(
          (v, t, t2) => _ParticipatingPartyAppearanceCopyWithImpl(v, t, t2));
}

abstract class ParticipatingPartyAppearanceCopyWith<
    $R,
    $In extends ParticipatingPartyAppearance,
    $Out> implements ClassCopyWith<$R, $In, $Out> {
  ParticipatingPartyCopyWith<$R, ParticipatingParty, ParticipatingParty>
      get participatingParty;
  LocationMarkerCopyWith<$R, LocationMarker, LocationMarker> get locationMarker;
  $R call(
      {String? id,
      ParticipatingParty? participatingParty,
      LocationMarker? locationMarker});
  ParticipatingPartyAppearanceCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _ParticipatingPartyAppearanceCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, ParticipatingPartyAppearance, $Out>
    implements
        ParticipatingPartyAppearanceCopyWith<$R, ParticipatingPartyAppearance,
            $Out> {
  _ParticipatingPartyAppearanceCopyWithImpl(
      super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<ParticipatingPartyAppearance> $mapper =
      ParticipatingPartyAppearanceMapper.ensureInitialized();
  @override
  ParticipatingPartyCopyWith<$R, ParticipatingParty, ParticipatingParty>
      get participatingParty => $value.participatingParty.copyWith
          .$chain((v) => call(participatingParty: v));
  @override
  LocationMarkerCopyWith<$R, LocationMarker, LocationMarker>
      get locationMarker =>
          $value.locationMarker.copyWith.$chain((v) => call(locationMarker: v));
  @override
  $R call(
          {String? id,
          ParticipatingParty? participatingParty,
          LocationMarker? locationMarker}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (participatingParty != null) #participatingParty: participatingParty,
        if (locationMarker != null) #locationMarker: locationMarker
      }));
  @override
  ParticipatingPartyAppearance $make(CopyWithData data) =>
      ParticipatingPartyAppearance(
          id: data.get(#id, or: $value.id),
          participatingParty:
              data.get(#participatingParty, or: $value.participatingParty),
          locationMarker: data.get(#locationMarker, or: $value.locationMarker));

  @override
  ParticipatingPartyAppearanceCopyWith<$R2, ParticipatingPartyAppearance, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _ParticipatingPartyAppearanceCopyWithImpl($value, $cast, t);
}
