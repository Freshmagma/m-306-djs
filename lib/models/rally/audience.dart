import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';
import 'package:gibz_mobileapp/models/rally/participating_party.dart';

part 'audience.mapper.dart';

@MappableClass()
class Audience extends Equatable with AudienceMappable {
  final String id;
  final String title;
  final List<ParticipatingParty> participatingParties;

  const Audience({
    required this.id,
    required this.title,
    required this.participatingParties,
  });

  @override
  List<Object?> get props => [id];
}
