import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

part 'rally_stage.mapper.dart';

@MappableClass()
class RallyStage extends Equatable with RallyStageMappable {
  final String id;
  final String rallyId;
  final String stageId;

  const RallyStage({
    required this.id,
    required this.rallyId,
    required this.stageId,
  });

  @override
  List<Object?> get props => [id];
}
