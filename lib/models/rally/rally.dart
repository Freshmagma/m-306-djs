import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';
import 'assignment.dart';
import 'rally_stage.dart';

part 'rally.mapper.dart';

@MappableClass()
class Rally extends Equatable with RallyMappable {
  final String id;
  final String title;
  final List<RallyStage> rallyStages;
  final List<Assignment> assignments;

  const Rally({
    required this.id,
    required this.title,
    required this.rallyStages,
    required this.assignments,
  });

  @override
  List<Object?> get props => [id];
}
