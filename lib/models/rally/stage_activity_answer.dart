import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

part 'stage_activity_answer.mapper.dart';

@MappableClass()
class StageActivityAnswer extends Equatable with StageActivityAnswerMappable {
  final String id;
  final String answerText;

  const StageActivityAnswer({required this.id, required this.answerText});

  @override
  List<Object?> get props => [id];
}
