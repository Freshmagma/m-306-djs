// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'successful_rally_join_response.dart';

class SuccessfulRallyJoinResponseMapper
    extends ClassMapperBase<SuccessfulRallyJoinResponse> {
  SuccessfulRallyJoinResponseMapper._();

  static SuccessfulRallyJoinResponseMapper? _instance;
  static SuccessfulRallyJoinResponseMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = SuccessfulRallyJoinResponseMapper._());
      ParticipatingPartyMapper.ensureInitialized();
      RallyMapper.ensureInitialized();
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'SuccessfulRallyJoinResponse';

  static ParticipatingParty _$participatingParty(
          SuccessfulRallyJoinResponse v) =>
      v.participatingParty;
  static const Field<SuccessfulRallyJoinResponse, ParticipatingParty>
      _f$participatingParty = Field('participatingParty', _$participatingParty);
  static Rally _$rally(SuccessfulRallyJoinResponse v) => v.rally;
  static const Field<SuccessfulRallyJoinResponse, Rally> _f$rally =
      Field('rally', _$rally);

  @override
  final Map<Symbol, Field<SuccessfulRallyJoinResponse, dynamic>> fields =
      const {
    #participatingParty: _f$participatingParty,
    #rally: _f$rally,
  };

  static SuccessfulRallyJoinResponse _instantiate(DecodingData data) {
    return SuccessfulRallyJoinResponse(
        participatingParty: data.dec(_f$participatingParty),
        rally: data.dec(_f$rally));
  }

  @override
  final Function instantiate = _instantiate;

  static SuccessfulRallyJoinResponse fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<SuccessfulRallyJoinResponse>(map));
  }

  static SuccessfulRallyJoinResponse fromJson(String json) {
    return _guard((c) => c.fromJson<SuccessfulRallyJoinResponse>(json));
  }
}

mixin SuccessfulRallyJoinResponseMappable {
  String toJson() {
    return SuccessfulRallyJoinResponseMapper._guard(
        (c) => c.toJson(this as SuccessfulRallyJoinResponse));
  }

  Map<String, dynamic> toMap() {
    return SuccessfulRallyJoinResponseMapper._guard(
        (c) => c.toMap(this as SuccessfulRallyJoinResponse));
  }

  SuccessfulRallyJoinResponseCopyWith<SuccessfulRallyJoinResponse,
          SuccessfulRallyJoinResponse, SuccessfulRallyJoinResponse>
      get copyWith => _SuccessfulRallyJoinResponseCopyWithImpl(
          this as SuccessfulRallyJoinResponse, $identity, $identity);
  @override
  String toString() {
    return SuccessfulRallyJoinResponseMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            SuccessfulRallyJoinResponseMapper._guard(
                (c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return SuccessfulRallyJoinResponseMapper._guard((c) => c.hash(this));
  }
}

extension SuccessfulRallyJoinResponseValueCopy<$R, $Out>
    on ObjectCopyWith<$R, SuccessfulRallyJoinResponse, $Out> {
  SuccessfulRallyJoinResponseCopyWith<$R, SuccessfulRallyJoinResponse, $Out>
      get $asSuccessfulRallyJoinResponse => $base
          .as((v, t, t2) => _SuccessfulRallyJoinResponseCopyWithImpl(v, t, t2));
}

abstract class SuccessfulRallyJoinResponseCopyWith<
    $R,
    $In extends SuccessfulRallyJoinResponse,
    $Out> implements ClassCopyWith<$R, $In, $Out> {
  ParticipatingPartyCopyWith<$R, ParticipatingParty, ParticipatingParty>
      get participatingParty;
  RallyCopyWith<$R, Rally, Rally> get rally;
  $R call({ParticipatingParty? participatingParty, Rally? rally});
  SuccessfulRallyJoinResponseCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _SuccessfulRallyJoinResponseCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, SuccessfulRallyJoinResponse, $Out>
    implements
        SuccessfulRallyJoinResponseCopyWith<$R, SuccessfulRallyJoinResponse,
            $Out> {
  _SuccessfulRallyJoinResponseCopyWithImpl(
      super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<SuccessfulRallyJoinResponse> $mapper =
      SuccessfulRallyJoinResponseMapper.ensureInitialized();
  @override
  ParticipatingPartyCopyWith<$R, ParticipatingParty, ParticipatingParty>
      get participatingParty => $value.participatingParty.copyWith
          .$chain((v) => call(participatingParty: v));
  @override
  RallyCopyWith<$R, Rally, Rally> get rally =>
      $value.rally.copyWith.$chain((v) => call(rally: v));
  @override
  $R call({ParticipatingParty? participatingParty, Rally? rally}) =>
      $apply(FieldCopyWithData({
        if (participatingParty != null) #participatingParty: participatingParty,
        if (rally != null) #rally: rally
      }));
  @override
  SuccessfulRallyJoinResponse $make(CopyWithData data) =>
      SuccessfulRallyJoinResponse(
          participatingParty:
              data.get(#participatingParty, or: $value.participatingParty),
          rally: data.get(#rally, or: $value.rally));

  @override
  SuccessfulRallyJoinResponseCopyWith<$R2, SuccessfulRallyJoinResponse, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _SuccessfulRallyJoinResponseCopyWithImpl($value, $cast, t);
}
