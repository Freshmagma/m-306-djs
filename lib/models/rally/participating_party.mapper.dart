// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'participating_party.dart';

class ParticipatingPartyMapper extends ClassMapperBase<ParticipatingParty> {
  ParticipatingPartyMapper._();

  static ParticipatingPartyMapper? _instance;
  static ParticipatingPartyMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = ParticipatingPartyMapper._());
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'ParticipatingParty';

  static String _$id(ParticipatingParty v) => v.id;
  static const Field<ParticipatingParty, String> _f$id = Field('id', _$id);
  static String? _$title(ParticipatingParty v) => v.title;
  static const Field<ParticipatingParty, String> _f$title =
      Field('title', _$title);

  @override
  final Map<Symbol, Field<ParticipatingParty, dynamic>> fields = const {
    #id: _f$id,
    #title: _f$title,
  };

  static ParticipatingParty _instantiate(DecodingData data) {
    return ParticipatingParty(id: data.dec(_f$id), title: data.dec(_f$title));
  }

  @override
  final Function instantiate = _instantiate;

  static ParticipatingParty fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<ParticipatingParty>(map));
  }

  static ParticipatingParty fromJson(String json) {
    return _guard((c) => c.fromJson<ParticipatingParty>(json));
  }
}

mixin ParticipatingPartyMappable {
  String toJson() {
    return ParticipatingPartyMapper._guard(
        (c) => c.toJson(this as ParticipatingParty));
  }

  Map<String, dynamic> toMap() {
    return ParticipatingPartyMapper._guard(
        (c) => c.toMap(this as ParticipatingParty));
  }

  ParticipatingPartyCopyWith<ParticipatingParty, ParticipatingParty,
          ParticipatingParty>
      get copyWith => _ParticipatingPartyCopyWithImpl(
          this as ParticipatingParty, $identity, $identity);
  @override
  String toString() {
    return ParticipatingPartyMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            ParticipatingPartyMapper._guard((c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return ParticipatingPartyMapper._guard((c) => c.hash(this));
  }
}

extension ParticipatingPartyValueCopy<$R, $Out>
    on ObjectCopyWith<$R, ParticipatingParty, $Out> {
  ParticipatingPartyCopyWith<$R, ParticipatingParty, $Out>
      get $asParticipatingParty =>
          $base.as((v, t, t2) => _ParticipatingPartyCopyWithImpl(v, t, t2));
}

abstract class ParticipatingPartyCopyWith<$R, $In extends ParticipatingParty,
    $Out> implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? id, String? title});
  ParticipatingPartyCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _ParticipatingPartyCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, ParticipatingParty, $Out>
    implements ParticipatingPartyCopyWith<$R, ParticipatingParty, $Out> {
  _ParticipatingPartyCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<ParticipatingParty> $mapper =
      ParticipatingPartyMapper.ensureInitialized();
  @override
  $R call({String? id, Object? title = $none}) => $apply(FieldCopyWithData(
      {if (id != null) #id: id, if (title != $none) #title: title}));
  @override
  ParticipatingParty $make(CopyWithData data) => ParticipatingParty(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title));

  @override
  ParticipatingPartyCopyWith<$R2, ParticipatingParty, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _ParticipatingPartyCopyWithImpl($value, $cast, t);
}
