import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

part 'location_marker.mapper.dart';

@MappableClass(discriminatorKey: "type")
abstract class LocationMarker extends Equatable with LocationMarkerMappable {
  final String id;

  const LocationMarker(this.id);

  @override
  List<Object?> get props => [id];
}

@MappableClass(discriminatorValue: "IBeaconLocationMarker")
class IBeaconLocationMarker extends LocationMarker
    with IBeaconLocationMarkerMappable {
  final String uuid;
  final int? major;
  final int? minor;

  const IBeaconLocationMarker(
    String id, {
    required this.uuid,
    required this.major,
    required this.minor,
  }) : super(id);
}

@MappableClass(discriminatorValue: "QRCodeLocationMarker")
class QRCodeLocationMarker extends LocationMarker
    with QRCodeLocationMarkerMappable {
  final String content;

  const QRCodeLocationMarker(String id, {required this.content}) : super(id);
}
