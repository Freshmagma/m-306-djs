// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'rally_stage.dart';

class RallyStageMapper extends ClassMapperBase<RallyStage> {
  RallyStageMapper._();

  static RallyStageMapper? _instance;
  static RallyStageMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = RallyStageMapper._());
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'RallyStage';

  static String _$id(RallyStage v) => v.id;
  static const Field<RallyStage, String> _f$id = Field('id', _$id);
  static String _$rallyId(RallyStage v) => v.rallyId;
  static const Field<RallyStage, String> _f$rallyId =
      Field('rallyId', _$rallyId);
  static String _$stageId(RallyStage v) => v.stageId;
  static const Field<RallyStage, String> _f$stageId =
      Field('stageId', _$stageId);

  @override
  final Map<Symbol, Field<RallyStage, dynamic>> fields = const {
    #id: _f$id,
    #rallyId: _f$rallyId,
    #stageId: _f$stageId,
  };

  static RallyStage _instantiate(DecodingData data) {
    return RallyStage(
        id: data.dec(_f$id),
        rallyId: data.dec(_f$rallyId),
        stageId: data.dec(_f$stageId));
  }

  @override
  final Function instantiate = _instantiate;

  static RallyStage fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<RallyStage>(map));
  }

  static RallyStage fromJson(String json) {
    return _guard((c) => c.fromJson<RallyStage>(json));
  }
}

mixin RallyStageMappable {
  String toJson() {
    return RallyStageMapper._guard((c) => c.toJson(this as RallyStage));
  }

  Map<String, dynamic> toMap() {
    return RallyStageMapper._guard((c) => c.toMap(this as RallyStage));
  }

  RallyStageCopyWith<RallyStage, RallyStage, RallyStage> get copyWith =>
      _RallyStageCopyWithImpl(this as RallyStage, $identity, $identity);
  @override
  String toString() {
    return RallyStageMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            RallyStageMapper._guard((c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return RallyStageMapper._guard((c) => c.hash(this));
  }
}

extension RallyStageValueCopy<$R, $Out>
    on ObjectCopyWith<$R, RallyStage, $Out> {
  RallyStageCopyWith<$R, RallyStage, $Out> get $asRallyStage =>
      $base.as((v, t, t2) => _RallyStageCopyWithImpl(v, t, t2));
}

abstract class RallyStageCopyWith<$R, $In extends RallyStage, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? id, String? rallyId, String? stageId});
  RallyStageCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _RallyStageCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, RallyStage, $Out>
    implements RallyStageCopyWith<$R, RallyStage, $Out> {
  _RallyStageCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<RallyStage> $mapper =
      RallyStageMapper.ensureInitialized();
  @override
  $R call({String? id, String? rallyId, String? stageId}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (rallyId != null) #rallyId: rallyId,
        if (stageId != null) #stageId: stageId
      }));
  @override
  RallyStage $make(CopyWithData data) => RallyStage(
      id: data.get(#id, or: $value.id),
      rallyId: data.get(#rallyId, or: $value.rallyId),
      stageId: data.get(#stageId, or: $value.stageId));

  @override
  RallyStageCopyWith<$R2, RallyStage, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _RallyStageCopyWithImpl($value, $cast, t);
}
