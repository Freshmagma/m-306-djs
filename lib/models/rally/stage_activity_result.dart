import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';
import 'package:gibz_mobileapp/models/rally/participating_party_answer.dart';

part 'stage_activity_result.mapper.dart';

@MappableClass()
class StageActivityResult extends Equatable with StageActivityResultMappable {
  final String id;
  final List<ParticipatingPartyAnswer> answers;
  final int awardedPoints;
  final int totalPoints;
  final int potentialHighscore;
  final DateTime momentOfCompletion;

  const StageActivityResult({
    required this.id,
    required this.answers,
    required this.awardedPoints,
    required this.totalPoints,
    required this.potentialHighscore,
    required this.momentOfCompletion,
  });

  @override
  List<Object?> get props => [id];
}
