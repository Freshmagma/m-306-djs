import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

import 'student.dart';

part 'token.mapper.dart';

@MappableClass(caseStyle: CaseStyle.snakeCase)
class Token extends Equatable with TokenMappable {
  @MappableField(key: 'token')
  final String id;
  final bool isUsed;
  final Student student;

  const Token({
    required this.id,
    required this.isUsed,
    required this.student,
  });

  @override
  List<Object?> get props => [id];
}
