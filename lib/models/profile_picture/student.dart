import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

import 'cohort.dart';

part 'student.mapper.dart';

@MappableClass(caseStyle: CaseStyle.snakeCase)
class Student extends Equatable with StudentMappable {
  final String id;
  final String gib;
  final String username;
  final String firstName;
  final String lastName;
  @MappableField(key: 'class')
  final Cohort cohort;

  const Student({
    required this.id,
    required this.gib,
    required this.username,
    required this.firstName,
    required this.lastName,
    required this.cohort,
  });

  @override
  List<Object?> get props => [id];
}
