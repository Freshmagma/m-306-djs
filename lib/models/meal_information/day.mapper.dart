// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'day.dart';

class DayMapper extends ClassMapperBase<Day> {
  DayMapper._();

  static DayMapper? _instance;
  static DayMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = DayMapper._());
      MealMapper.ensureInitialized();
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'Day';

  static String _$id(Day v) => v.id;
  static const Field<Day, String> _f$id = Field('id', _$id);
  static DateTime _$date(Day v) => v.date;
  static const Field<Day, DateTime> _f$date = Field('date', _$date);
  static List<Meal> _$menus(Day v) => v.menus;
  static const Field<Day, List<Meal>> _f$menus = Field('menus', _$menus);

  @override
  final Map<Symbol, Field<Day, dynamic>> fields = const {
    #id: _f$id,
    #date: _f$date,
    #menus: _f$menus,
  };

  static Day _instantiate(DecodingData data) {
    return Day(
        id: data.dec(_f$id),
        date: data.dec(_f$date),
        menus: data.dec(_f$menus));
  }

  @override
  final Function instantiate = _instantiate;

  static Day fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<Day>(map));
  }

  static Day fromJson(String json) {
    return _guard((c) => c.fromJson<Day>(json));
  }
}

mixin DayMappable {
  String toJson() {
    return DayMapper._guard((c) => c.toJson(this as Day));
  }

  Map<String, dynamic> toMap() {
    return DayMapper._guard((c) => c.toMap(this as Day));
  }

  DayCopyWith<Day, Day, Day> get copyWith =>
      _DayCopyWithImpl(this as Day, $identity, $identity);
  @override
  String toString() {
    return DayMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            DayMapper._guard((c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return DayMapper._guard((c) => c.hash(this));
  }
}

extension DayValueCopy<$R, $Out> on ObjectCopyWith<$R, Day, $Out> {
  DayCopyWith<$R, Day, $Out> get $asDay =>
      $base.as((v, t, t2) => _DayCopyWithImpl(v, t, t2));
}

abstract class DayCopyWith<$R, $In extends Day, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  ListCopyWith<$R, Meal, MealCopyWith<$R, Meal, Meal>> get menus;
  $R call({String? id, DateTime? date, List<Meal>? menus});
  DayCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _DayCopyWithImpl<$R, $Out> extends ClassCopyWithBase<$R, Day, $Out>
    implements DayCopyWith<$R, Day, $Out> {
  _DayCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<Day> $mapper = DayMapper.ensureInitialized();
  @override
  ListCopyWith<$R, Meal, MealCopyWith<$R, Meal, Meal>> get menus =>
      ListCopyWith(
          $value.menus, (v, t) => v.copyWith.$chain(t), (v) => call(menus: v));
  @override
  $R call({String? id, DateTime? date, List<Meal>? menus}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (date != null) #date: date,
        if (menus != null) #menus: menus
      }));
  @override
  Day $make(CopyWithData data) => Day(
      id: data.get(#id, or: $value.id),
      date: data.get(#date, or: $value.date),
      menus: data.get(#menus, or: $value.menus));

  @override
  DayCopyWith<$R2, Day, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
      _DayCopyWithImpl($value, $cast, t);
}
