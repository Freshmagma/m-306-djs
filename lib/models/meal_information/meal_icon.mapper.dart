// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'meal_icon.dart';

class MealIconMapper extends ClassMapperBase<MealIcon> {
  MealIconMapper._();

  static MealIconMapper? _instance;
  static MealIconMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = MealIconMapper._());
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'MealIcon';

  static String _$id(MealIcon v) => v.id;
  static const Field<MealIcon, String> _f$id = Field('id', _$id);
  static String _$sourceUri(MealIcon v) => v.sourceUri;
  static const Field<MealIcon, String> _f$sourceUri =
      Field('sourceUri', _$sourceUri);
  static String _$altText(MealIcon v) => v.altText;
  static const Field<MealIcon, String> _f$altText = Field('altText', _$altText);

  @override
  final Map<Symbol, Field<MealIcon, dynamic>> fields = const {
    #id: _f$id,
    #sourceUri: _f$sourceUri,
    #altText: _f$altText,
  };

  static MealIcon _instantiate(DecodingData data) {
    return MealIcon(
        id: data.dec(_f$id),
        sourceUri: data.dec(_f$sourceUri),
        altText: data.dec(_f$altText));
  }

  @override
  final Function instantiate = _instantiate;

  static MealIcon fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<MealIcon>(map));
  }

  static MealIcon fromJson(String json) {
    return _guard((c) => c.fromJson<MealIcon>(json));
  }
}

mixin MealIconMappable {
  String toJson() {
    return MealIconMapper._guard((c) => c.toJson(this as MealIcon));
  }

  Map<String, dynamic> toMap() {
    return MealIconMapper._guard((c) => c.toMap(this as MealIcon));
  }

  MealIconCopyWith<MealIcon, MealIcon, MealIcon> get copyWith =>
      _MealIconCopyWithImpl(this as MealIcon, $identity, $identity);
  @override
  String toString() {
    return MealIconMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            MealIconMapper._guard((c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return MealIconMapper._guard((c) => c.hash(this));
  }
}

extension MealIconValueCopy<$R, $Out> on ObjectCopyWith<$R, MealIcon, $Out> {
  MealIconCopyWith<$R, MealIcon, $Out> get $asMealIcon =>
      $base.as((v, t, t2) => _MealIconCopyWithImpl(v, t, t2));
}

abstract class MealIconCopyWith<$R, $In extends MealIcon, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? id, String? sourceUri, String? altText});
  MealIconCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _MealIconCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, MealIcon, $Out>
    implements MealIconCopyWith<$R, MealIcon, $Out> {
  _MealIconCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<MealIcon> $mapper =
      MealIconMapper.ensureInitialized();
  @override
  $R call({String? id, String? sourceUri, String? altText}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (sourceUri != null) #sourceUri: sourceUri,
        if (altText != null) #altText: altText
      }));
  @override
  MealIcon $make(CopyWithData data) => MealIcon(
      id: data.get(#id, or: $value.id),
      sourceUri: data.get(#sourceUri, or: $value.sourceUri),
      altText: data.get(#altText, or: $value.altText));

  @override
  MealIconCopyWith<$R2, MealIcon, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _MealIconCopyWithImpl($value, $cast, t);
}
