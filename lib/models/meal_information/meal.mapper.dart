// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'meal.dart';

class MealMapper extends ClassMapperBase<Meal> {
  MealMapper._();

  static MealMapper? _instance;
  static MealMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = MealMapper._());
      NutritionalValueMapper.ensureInitialized();
      MealIconMapper.ensureInitialized();
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'Meal';

  static String _$id(Meal v) => v.id;
  static const Field<Meal, String> _f$id = Field('id', _$id);
  static String _$title(Meal v) => v.title;
  static const Field<Meal, String> _f$title = Field('title', _$title);
  static double _$priceInternal(Meal v) => v.priceInternal;
  static const Field<Meal, double> _f$priceInternal =
      Field('priceInternal', _$priceInternal, opt: true, def: 0.0);
  static double _$priceExternal(Meal v) => v.priceExternal;
  static const Field<Meal, double> _f$priceExternal =
      Field('priceExternal', _$priceExternal, opt: true, def: 0.0);
  static List<String> _$menuComponents(Meal v) => v.menuComponents;
  static const Field<Meal, List<String>> _f$menuComponents =
      Field('menuComponents', _$menuComponents);
  static List<String> _$allergenes(Meal v) => v.allergenes;
  static const Field<Meal, List<String>> _f$allergenes =
      Field('allergenes', _$allergenes);
  static String? _$balance(Meal v) => v.balance;
  static const Field<Meal, String> _f$balance =
      Field('balance', _$balance, opt: true);
  static String? _$environmentalImpact(Meal v) => v.environmentalImpact;
  static const Field<Meal, String> _f$environmentalImpact =
      Field('environmentalImpact', _$environmentalImpact, opt: true);
  static List<String> _$additionalInformation(Meal v) =>
      v.additionalInformation;
  static const Field<Meal, List<String>> _f$additionalInformation = Field(
      'additionalInformation', _$additionalInformation,
      opt: true, def: const <String>[]);
  static List<NutritionalValue> _$nutritionalValues(Meal v) =>
      v.nutritionalValues;
  static const Field<Meal, List<NutritionalValue>> _f$nutritionalValues =
      Field('nutritionalValues', _$nutritionalValues);
  static List<MealIcon> _$icons(Meal v) => v.icons;
  static const Field<Meal, List<MealIcon>> _f$icons =
      Field('icons', _$icons, opt: true, def: const <MealIcon>[]);

  @override
  final Map<Symbol, Field<Meal, dynamic>> fields = const {
    #id: _f$id,
    #title: _f$title,
    #priceInternal: _f$priceInternal,
    #priceExternal: _f$priceExternal,
    #menuComponents: _f$menuComponents,
    #allergenes: _f$allergenes,
    #balance: _f$balance,
    #environmentalImpact: _f$environmentalImpact,
    #additionalInformation: _f$additionalInformation,
    #nutritionalValues: _f$nutritionalValues,
    #icons: _f$icons,
  };

  static Meal _instantiate(DecodingData data) {
    return Meal(
        id: data.dec(_f$id),
        title: data.dec(_f$title),
        priceInternal: data.dec(_f$priceInternal),
        priceExternal: data.dec(_f$priceExternal),
        menuComponents: data.dec(_f$menuComponents),
        allergenes: data.dec(_f$allergenes),
        balance: data.dec(_f$balance),
        environmentalImpact: data.dec(_f$environmentalImpact),
        additionalInformation: data.dec(_f$additionalInformation),
        nutritionalValues: data.dec(_f$nutritionalValues),
        icons: data.dec(_f$icons));
  }

  @override
  final Function instantiate = _instantiate;

  static Meal fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<Meal>(map));
  }

  static Meal fromJson(String json) {
    return _guard((c) => c.fromJson<Meal>(json));
  }
}

mixin MealMappable {
  String toJson() {
    return MealMapper._guard((c) => c.toJson(this as Meal));
  }

  Map<String, dynamic> toMap() {
    return MealMapper._guard((c) => c.toMap(this as Meal));
  }

  MealCopyWith<Meal, Meal, Meal> get copyWith =>
      _MealCopyWithImpl(this as Meal, $identity, $identity);
  @override
  String toString() {
    return MealMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            MealMapper._guard((c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return MealMapper._guard((c) => c.hash(this));
  }
}

extension MealValueCopy<$R, $Out> on ObjectCopyWith<$R, Meal, $Out> {
  MealCopyWith<$R, Meal, $Out> get $asMeal =>
      $base.as((v, t, t2) => _MealCopyWithImpl(v, t, t2));
}

abstract class MealCopyWith<$R, $In extends Meal, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>>
      get menuComponents;
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>> get allergenes;
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>>
      get additionalInformation;
  ListCopyWith<$R, NutritionalValue,
          NutritionalValueCopyWith<$R, NutritionalValue, NutritionalValue>>
      get nutritionalValues;
  ListCopyWith<$R, MealIcon, MealIconCopyWith<$R, MealIcon, MealIcon>>
      get icons;
  $R call(
      {String? id,
      String? title,
      double? priceInternal,
      double? priceExternal,
      List<String>? menuComponents,
      List<String>? allergenes,
      String? balance,
      String? environmentalImpact,
      List<String>? additionalInformation,
      List<NutritionalValue>? nutritionalValues,
      List<MealIcon>? icons});
  MealCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _MealCopyWithImpl<$R, $Out> extends ClassCopyWithBase<$R, Meal, $Out>
    implements MealCopyWith<$R, Meal, $Out> {
  _MealCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<Meal> $mapper = MealMapper.ensureInitialized();
  @override
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>>
      get menuComponents => ListCopyWith(
          $value.menuComponents,
          (v, t) => ObjectCopyWith(v, $identity, t),
          (v) => call(menuComponents: v));
  @override
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>> get allergenes =>
      ListCopyWith($value.allergenes, (v, t) => ObjectCopyWith(v, $identity, t),
          (v) => call(allergenes: v));
  @override
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>>
      get additionalInformation => ListCopyWith(
          $value.additionalInformation,
          (v, t) => ObjectCopyWith(v, $identity, t),
          (v) => call(additionalInformation: v));
  @override
  ListCopyWith<$R, NutritionalValue,
          NutritionalValueCopyWith<$R, NutritionalValue, NutritionalValue>>
      get nutritionalValues => ListCopyWith($value.nutritionalValues,
          (v, t) => v.copyWith.$chain(t), (v) => call(nutritionalValues: v));
  @override
  ListCopyWith<$R, MealIcon, MealIconCopyWith<$R, MealIcon, MealIcon>>
      get icons => ListCopyWith(
          $value.icons, (v, t) => v.copyWith.$chain(t), (v) => call(icons: v));
  @override
  $R call(
          {String? id,
          String? title,
          double? priceInternal,
          double? priceExternal,
          List<String>? menuComponents,
          List<String>? allergenes,
          Object? balance = $none,
          Object? environmentalImpact = $none,
          List<String>? additionalInformation,
          List<NutritionalValue>? nutritionalValues,
          List<MealIcon>? icons}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (priceInternal != null) #priceInternal: priceInternal,
        if (priceExternal != null) #priceExternal: priceExternal,
        if (menuComponents != null) #menuComponents: menuComponents,
        if (allergenes != null) #allergenes: allergenes,
        if (balance != $none) #balance: balance,
        if (environmentalImpact != $none)
          #environmentalImpact: environmentalImpact,
        if (additionalInformation != null)
          #additionalInformation: additionalInformation,
        if (nutritionalValues != null) #nutritionalValues: nutritionalValues,
        if (icons != null) #icons: icons
      }));
  @override
  Meal $make(CopyWithData data) => Meal(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      priceInternal: data.get(#priceInternal, or: $value.priceInternal),
      priceExternal: data.get(#priceExternal, or: $value.priceExternal),
      menuComponents: data.get(#menuComponents, or: $value.menuComponents),
      allergenes: data.get(#allergenes, or: $value.allergenes),
      balance: data.get(#balance, or: $value.balance),
      environmentalImpact:
          data.get(#environmentalImpact, or: $value.environmentalImpact),
      additionalInformation:
          data.get(#additionalInformation, or: $value.additionalInformation),
      nutritionalValues:
          data.get(#nutritionalValues, or: $value.nutritionalValues),
      icons: data.get(#icons, or: $value.icons));

  @override
  MealCopyWith<$R2, Meal, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
      _MealCopyWithImpl($value, $cast, t);
}
