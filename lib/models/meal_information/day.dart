import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

import '../meal_information.dart';

part 'day.mapper.dart';

@MappableClass()
class Day extends Equatable with DayMappable {
  final String id;
  final DateTime date;
  final List<Meal> menus;

  List<Meal> get actualMenus =>
      menus.where((menu) => menu.title.toLowerCase() != 'news').toList();

  Meal get news =>
      menus.firstWhere((menu) => menu.title.toLowerCase() == 'news');

  const Day({
    required this.id,
    required this.date,
    required this.menus,
  });

  @override
  List<Object?> get props => [id];
}
