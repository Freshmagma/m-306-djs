// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element

part of 'nutritional_value.dart';

class NutritionalValueMapper extends ClassMapperBase<NutritionalValue> {
  NutritionalValueMapper._();

  static NutritionalValueMapper? _instance;
  static NutritionalValueMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = NutritionalValueMapper._());
    }
    return _instance!;
  }

  static T _guard<T>(T Function(MapperContainer) fn) {
    ensureInitialized();
    return fn(MapperContainer.globals);
  }

  @override
  final String id = 'NutritionalValue';

  static String _$id(NutritionalValue v) => v.id;
  static const Field<NutritionalValue, String> _f$id = Field('id', _$id);
  static String _$title(NutritionalValue v) => v.title;
  static const Field<NutritionalValue, String> _f$title =
      Field('title', _$title);
  static String _$value(NutritionalValue v) => v.value;
  static const Field<NutritionalValue, String> _f$value =
      Field('value', _$value);
  static int _$rowIndex(NutritionalValue v) => v.rowIndex;
  static const Field<NutritionalValue, int> _f$rowIndex =
      Field('rowIndex', _$rowIndex);

  @override
  final Map<Symbol, Field<NutritionalValue, dynamic>> fields = const {
    #id: _f$id,
    #title: _f$title,
    #value: _f$value,
    #rowIndex: _f$rowIndex,
  };

  static NutritionalValue _instantiate(DecodingData data) {
    return NutritionalValue(
        id: data.dec(_f$id),
        title: data.dec(_f$title),
        value: data.dec(_f$value),
        rowIndex: data.dec(_f$rowIndex));
  }

  @override
  final Function instantiate = _instantiate;

  static NutritionalValue fromMap(Map<String, dynamic> map) {
    return _guard((c) => c.fromMap<NutritionalValue>(map));
  }

  static NutritionalValue fromJson(String json) {
    return _guard((c) => c.fromJson<NutritionalValue>(json));
  }
}

mixin NutritionalValueMappable {
  String toJson() {
    return NutritionalValueMapper._guard(
        (c) => c.toJson(this as NutritionalValue));
  }

  Map<String, dynamic> toMap() {
    return NutritionalValueMapper._guard(
        (c) => c.toMap(this as NutritionalValue));
  }

  NutritionalValueCopyWith<NutritionalValue, NutritionalValue, NutritionalValue>
      get copyWith => _NutritionalValueCopyWithImpl(
          this as NutritionalValue, $identity, $identity);
  @override
  String toString() {
    return NutritionalValueMapper._guard((c) => c.asString(this));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (runtimeType == other.runtimeType &&
            NutritionalValueMapper._guard((c) => c.isEqual(this, other)));
  }

  @override
  int get hashCode {
    return NutritionalValueMapper._guard((c) => c.hash(this));
  }
}

extension NutritionalValueValueCopy<$R, $Out>
    on ObjectCopyWith<$R, NutritionalValue, $Out> {
  NutritionalValueCopyWith<$R, NutritionalValue, $Out>
      get $asNutritionalValue =>
          $base.as((v, t, t2) => _NutritionalValueCopyWithImpl(v, t, t2));
}

abstract class NutritionalValueCopyWith<$R, $In extends NutritionalValue, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? id, String? title, String? value, int? rowIndex});
  NutritionalValueCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _NutritionalValueCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, NutritionalValue, $Out>
    implements NutritionalValueCopyWith<$R, NutritionalValue, $Out> {
  _NutritionalValueCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<NutritionalValue> $mapper =
      NutritionalValueMapper.ensureInitialized();
  @override
  $R call({String? id, String? title, String? value, int? rowIndex}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (value != null) #value: value,
        if (rowIndex != null) #rowIndex: rowIndex
      }));
  @override
  NutritionalValue $make(CopyWithData data) => NutritionalValue(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      value: data.get(#value, or: $value.value),
      rowIndex: data.get(#rowIndex, or: $value.rowIndex));

  @override
  NutritionalValueCopyWith<$R2, NutritionalValue, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _NutritionalValueCopyWithImpl($value, $cast, t);
}
