import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

final defaultMarkdownStylesheet = MarkdownStyleSheet(
  p: const TextStyle(
    fontSize: 14,
  ),
  a: const TextStyle(
    color: Color.fromRGBO(0, 119, 192, 1),
    decoration: TextDecoration.underline,
    decorationStyle: TextDecorationStyle.dotted,
  ),
  h1: const TextStyle(
    color: Color.fromRGBO(0, 119, 192, 1),
    fontSize: 28,
  ),
  h2: const TextStyle(
    fontSize: 22,
  ),
  h3: const TextStyle(
    fontSize: 18,
  ),
  h4: const TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
  listIndent: 12,
  listBullet: const TextStyle(color: Colors.black54),
  code: TextStyle(
    fontSize: 14,
    backgroundColor: Colors.grey.shade100,
    color: Colors.black45,
    fontFamily: 'Courier New',
    fontWeight: FontWeight.bold,
  ),
  strong: const TextStyle(fontWeight: FontWeight.w600),
  h1Padding: const EdgeInsets.only(top: 10, bottom: 0),
  h2Padding: const EdgeInsets.only(top: 10, bottom: 0),
  h3Padding: const EdgeInsets.only(top: 10, bottom: 0),
  h4Padding: const EdgeInsets.only(top: 10, bottom: 0),
  pPadding: const EdgeInsets.only(top: 0, bottom: 7.5),
  blockSpacing: 4,
);
