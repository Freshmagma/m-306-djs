import 'package:flutter/material.dart';
import 'package:gibz_mobileapp/widgets/bottom_navigation_bar_item_icon.dart';
import 'package:go_router/go_router.dart';

class NavigationShell extends StatelessWidget {
  const NavigationShell({
    required this.child,
    super.key,
  });

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: child,
      extendBody: true,
      // False is required for /gibzrally/members
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: Container(
        clipBehavior: Clip.antiAlias,
        decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
            boxShadow: [
              BoxShadow(
                color: Color.fromRGBO(178, 178, 178, 0.25),
                blurRadius: 18,
                offset: Offset(0, 4),
              )
            ]),
        child: Theme(
          data: Theme.of(context).copyWith(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
          ),
          child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            backgroundColor: Colors.white,
            currentIndex: _calculateCurrentIndex(context),
            onTap: (int index) => _onItemTapped(context, index),
            items: [
              BottomNavigationBarItem(
                icon: BottomNavigationBarItemIcon(
                  iconPath: 'assets/icons/menu_home.svg',
                  color: Theme.of(context).colorScheme.secondary,
                ),
                activeIcon: BottomNavigationBarItemIcon(
                  iconPath: 'assets/icons/menu_home.svg',
                  color: Theme.of(context).colorScheme.primary,
                ),
                label: 'Dashboard',
              ),
              BottomNavigationBarItem(
                icon: BottomNavigationBarItemIcon(
                  iconPath: 'assets/icons/menu_studentsManual.svg',
                  color: Theme.of(context).colorScheme.secondary,
                ),
                activeIcon: BottomNavigationBarItemIcon(
                  iconPath: 'assets/icons/menu_studentsManual.svg',
                  color: Theme.of(context).colorScheme.primary,
                ),
                label: 'Handbuch',
              ),
              BottomNavigationBarItem(
                icon: BottomNavigationBarItemIcon(
                  iconPath: 'assets/icons/dashboard/meal_information.svg',
                  color: Theme.of(context).colorScheme.secondary,
                ),
                activeIcon: BottomNavigationBarItemIcon(
                  iconPath: 'assets/icons/dashboard/meal_information.svg',
                  color: Theme.of(context).colorScheme.primary,
                ),
                label: 'Menüplan',
              ),
            ],
          ),
        ),
      ),
    );
  }

  _calculateCurrentIndex(BuildContext context) {
    final String location = GoRouterState.of(context).uri.toString();
    if (location.startsWith('/students-manual')) {
      return 1;
    }
    if (location.startsWith('/meal-information')) {
      return 2;
    }

    return 0;
  }

  _onItemTapped(BuildContext context, int index) {
    String path = '/';
    switch (index) {
      case 1:
        path = '/students-manual';
        break;
      case 2:
        path = '/meal-information';
        break;
      default:
        path = '/';
        break;
    }
    GoRouter.of(context).go(path);
  }
}
