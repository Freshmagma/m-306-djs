import 'package:collection/collection.dart';
import 'package:gibz_mobileapp/models/students_manual.dart';

class ArticleInMemoryProvider {
  static const cachingTime = Duration(days: 1);
  DateTime? lastUpdate;
  List<Article> articles = [];

  void addData(List<Article> articles) {
    this.articles = articles;
    lastUpdate = DateTime.now();
  }

  List<Article>? getArticles() {
    if (lastUpdate == null ||
        DateTime.now().difference(lastUpdate!) > cachingTime) {
      return null;
    }
    return articles;
  }

  Article? getArticle(String articleId) {
    return articles.firstWhereOrNull((article) => article.id == articleId);
  }
}
