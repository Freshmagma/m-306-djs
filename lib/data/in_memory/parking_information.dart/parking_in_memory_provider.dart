import 'package:gibz_mobileapp/models/parking_information/car_park.dart';

class ParkingInMemoryProvider {
  static const cachingTime = Duration(minutes: 2);
  DateTime? lastUpdate;
  CarPark? carPark;

  void addData(CarPark carPark) {
    this.carPark = carPark;
    lastUpdate = DateTime.now();
  }

  CarPark? getCurrentAvailability() {
    if (lastUpdate == null ||
        DateTime.now().difference(lastUpdate!) > cachingTime) {
      return null;
    }
    return carPark;
  }
}
