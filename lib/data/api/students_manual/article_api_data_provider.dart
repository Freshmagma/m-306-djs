import 'package:dart_mappable/dart_mappable.dart';
import 'package:gibz_mobileapp/data/dio.dart';
import 'package:gibz_mobileapp/models/students_manual.dart';

class ArticleApiDataProvider {
  final DioClient _httpClient;

  ArticleApiDataProvider(this._httpClient);

  Future<List<Article>?> getArticles() async {
    try {
      final response = await _httpClient.get('/api/studentsmanual/v1/article');
      return MapperContainer.globals.fromJson<List<Article>>(response.data);
    } catch (e) {
      // TODO: Handle exception
      return null;
    }
  }

  Future<Article?> getArticle(String id) async {
    try {
      final response =
          await _httpClient.get('/api/studentsmanual/v1/article/$id');
      return Article.fromJson(response.data);
    } catch (e) {
      // TODO: Handle exception
      return null;
    }
  }
}
