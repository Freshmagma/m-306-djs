import 'package:gibz_mobileapp/data/dio.dart';
import 'package:gibz_mobileapp/models/rally/dto/stage_activity_result_creation_request.dart';
import 'package:gibz_mobileapp/models/rally/stage_activity_result.dart';

class StageActivityResultApi {
  final DioClient _httpClient;

  StageActivityResultApi(this._httpClient);

  Future<StageActivityResult?> submitStageActivityResult(
    String rallyId,
    String stageId,
    StageActivityResultCreationRequestDto payload,
  ) async {
    try {
      final response = await _httpClient.post(
        '/api/rally/v1/rally/$rallyId/stage/$stageId/stageActivityResult',
        data: payload.toJson(),
      );

      return StageActivityResultMapper.fromJson(response.data);
    } catch (error) {
      return null;
    }
  }
}
