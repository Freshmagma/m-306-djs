import 'dart:convert';

import 'package:gibz_mobileapp/data/dio.dart';
import 'package:gibz_mobileapp/models/rally/participating_party_appearance.dart';
import 'package:gibz_mobileapp/models/rally/stage.dart';

class RallyStageApi {
  final DioClient _httpClient;

  RallyStageApi(this._httpClient);

  Future<Stage?> getNext(
    String rallyId,
    String stageId,
    String participatingPartyId,
  ) async {
    final response = await _httpClient.get(
      '/api/rally/v1/rally/$rallyId/stage/$stageId/next/$participatingPartyId',
    );
    if (response.statusCode == 204) {
      return null;
    }

    return StageMapper.fromJson(response.data);
  }

  Future<ParticipatingPartyAppearance> addParticipatingPartyAppearance({
    required String rallyId,
    required String stageId,
    required String participatingPartyId,
    required String locationMarkerId,
  }) async {
    final response = await _httpClient.post(
      '/api/rally/v1/rally/$rallyId/stage/$stageId/appearance',
      data: jsonEncode({
        "participatingPartyId": participatingPartyId,
        "locationMarkerId": locationMarkerId,
      }),
    );

    return ParticipatingPartyAppearanceMapper.fromJson(response.data);
  }
}
