import 'dart:convert';

import 'package:gibz_mobileapp/data/dio.dart';
import 'package:gibz_mobileapp/models/rally/dto/participating_party_update_request_dto.dart';
import 'package:gibz_mobileapp/models/rally/stage.dart';
import 'package:gibz_mobileapp/models/rally/successful_rally_join_response.dart';
import 'package:gibz_mobileapp/state_management/progress_indicator_cubit.dart';

class ParticipatingPartyApi {
  final DioClient _httpClient;
  final ProgressIndicatorCubit _progressIndicatorCubit;

  ParticipatingPartyApi(this._httpClient, this._progressIndicatorCubit);

  Future<SuccessfulRallyJoinResponse?> createParticipatingParty(
      String joiningCode) async {
    final payload = jsonEncode({"joiningCode": joiningCode});
    try {
      final response = await _httpClient.post(
        '/api/rally/v1/party',
        data: payload,
      );

      if ((response.statusCode ?? 999) > 299) {
        _progressIndicatorCubit.finish(LoadingAspect.joiningRally);
        return null;
      }

      _progressIndicatorCubit.finish(LoadingAspect.joiningRally);
      return SuccessfulRallyJoinResponseMapper.fromJson(response.data);
    } catch (error) {
      _progressIndicatorCubit.finish(LoadingAspect.joiningRally);
      return null;
    } finally {
      _progressIndicatorCubit.finish(LoadingAspect.joiningRally);
    }
  }

  Future<Stage?> updateParticipatingParty({
    required String id,
    required String title,
    required List<String> names,
  }) async {
    final updateRequest = ParticipatingPartyUpdateRequestDto(
      id: id,
      title: title,
      names: names,
    );
    final payload = updateRequest.toJson();
    final response =
        await _httpClient.put('/api/rally/v1/party/$id', data: payload);

    if (response.statusCode == 204) {
      return null;
    }

    return StageMapper.fromJson(response.data);
  }
}
