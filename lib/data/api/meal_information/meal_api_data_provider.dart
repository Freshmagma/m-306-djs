import 'package:dart_mappable/dart_mappable.dart';
import 'package:gibz_mobileapp/data/dio.dart';
import 'package:gibz_mobileapp/models/meal_information.dart';

class MealApiDataProvider {
  final DioClient _httpClient;

  MealApiDataProvider(this._httpClient);

  Future<List<Day>?> getMealsForWholeWeek() async {
    try {
      final response =
          await _httpClient.get('/api/mealInformation/v1/menu?wholeWeek=true');
      return MapperContainer.globals.fromJson<List<Day>>(response.data);
    } catch (e) {
      // TODO: Handle exception
      return null;
    }
  }
}
