import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:gibz_mobileapp/data/dio.dart';
import 'package:gibz_mobileapp/models/profile_picture.dart';
import 'package:http_parser/http_parser.dart';

class ProfilePictureApi {
  final DioClient _httpClient;

  ProfilePictureApi(this._httpClient);

  Future<Token?> getToken(String tokenId) async {
    try {
      final response =
          await _httpClient.get('/bffw/api/profile-picture/v1/token/$tokenId');
      final token = TokenMapper.fromJson(response.data);
      return token;
    } catch (e) {
      // TODO: Handle exception.

      // Such an exception might occur when the token has already been used before.
      // The corresponding http status code is 409 (Conflict).
      return null;
    }
  }

  Future<bool> submitPicture(String token, Uint8List image) async {
    try {
      final picture = MultipartFile.fromBytes(
        image,
        contentType: MediaType('image', 'png'),
      );

      final formData = FormData.fromMap({
        'token': token,
        'pfp': picture,
      });

      final response = await _httpClient.post(
        '/bffw/api/profile-picture/v1/images/upload',
        data: formData,
      );

      return response.statusCode == 201;
    } catch (e) {
      return false;
    }
  }
}
