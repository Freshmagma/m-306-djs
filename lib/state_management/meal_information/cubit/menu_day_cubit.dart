import 'dart:math';

import 'package:flutter_bloc/flutter_bloc.dart';

class MenuDayCubit extends Cubit<int> {
  MenuDayCubit() : super(min(DateTime.now().weekday - 1, 4));

  static const List<String> weekDays = ['MO', 'DI', 'MI', 'DO', 'FR'];

  void setActiveDayIndex(int index) {
    emit(index);
  }

  void setActiveWeekday(String weekDay) {
    int index = weekDays.contains(weekDay.toUpperCase())
        ? weekDays.indexOf(weekDay.toUpperCase())
        : 0;
    emit(index);
  }
}
