part of 'stage_video_duration_cubit.dart';

abstract class StageVideoDurationState extends Equatable {
  const StageVideoDurationState();

  @override
  List<Object> get props => [];
}

class StageVideoDurationInitial extends StageVideoDurationState {}

class KnownStageVideoDuration extends StageVideoDurationState {
  final Duration videoDuration;

  Duration get roundedVideoDuration =>
      videoDuration.ceil(const Duration(minutes: 1));

  const KnownStageVideoDuration(this.videoDuration);

  @override
  List<Object> get props => [videoDuration];
}

class UnknownStageVideoDuration extends StageVideoDurationState {}
