import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

part 'stage_video_state.dart';

class StageVideoCubit extends Cubit<StageVideoState> {
  StageVideoCubit() : super(const StageVideoInitial(showControls: true));

  bool _showControls = true;
  bool _isPlaying = false;
  Timer? hideControlsTimer;

  @override
  Future<void> close() {
    hideControlsTimer?.cancel();
    return super.close();
  }

  void hideControls() {
    _showControls = false;
    final newState =
        StageVideoState.fromState(state, showControls: _showControls);
    emit(newState);
  }

  void toggleControls() {
    _showControls = !_showControls;
    final newState =
        StageVideoState.fromState(state, showControls: _showControls);
    emit(newState);

    if (_showControls) scheduleControlsToHide(const Duration(seconds: 2));
  }

  void togglePlay() {
    _isPlaying = !_isPlaying;
    StageVideoState newStage;
    if (_isPlaying) {
      newStage = StageVideoPlaying(showControls: _showControls);
      scheduleControlsToHide(const Duration(milliseconds: 1500));
    } else {
      newStage = StageVideoPaused(showControls: _showControls);
      hideControlsTimer?.cancel();
    }
    emit(newStage);
  }

  void scheduleControlsToHide(Duration duration) {
    hideControlsTimer?.cancel();
    hideControlsTimer = Timer(duration, hideControls);
  }
}
