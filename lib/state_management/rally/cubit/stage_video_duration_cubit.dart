import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:gibz_mobileapp/utilities/duration_extension_ceil.dart';

part 'stage_video_duration_state.dart';

class StageVideoDurationCubit extends Cubit<StageVideoDurationState> {
  StageVideoDurationCubit() : super(StageVideoDurationInitial());

  void setDuration(Duration videoDuration) {
    if (videoDuration == Duration.zero) {
      emit(UnknownStageVideoDuration());
    } else {
      emit(KnownStageVideoDuration(videoDuration));
    }
  }
}
