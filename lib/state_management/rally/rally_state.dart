part of 'rally_bloc.dart';

abstract class RallyState extends Equatable {
  const RallyState();

  @override
  List<Object?> get props => [];
}

class RallyInitial extends RallyState {
  final String? message;

  const RallyInitial({this.message});

  @override
  List<Object?> get props => [message];
}

class PendingTeamNameCompletion extends RallyState {}

class PendingTeamMembersCompletion extends RallyState {}

class PendingBluetoothPermissionGrant extends RallyState {
  final PermissionStatus bluetoothPermission;
  final ServiceStatus bluetoothService;
  final PermissionStatus bluetoothScanPermission;

  const PendingBluetoothPermissionGrant({
    required this.bluetoothPermission,
    required this.bluetoothService,
    required this.bluetoothScanPermission,
  });

  @override
  List<Object?> get props => [
        bluetoothPermission,
        bluetoothService,
        bluetoothScanPermission,
      ];
}

class PendingLocationPermissionGrant extends RallyState {}

abstract class StageInvolvedState extends RallyState {
  final Stage stage;

  const StageInvolvedState(this.stage);

  @override
  List<Object?> get props => [stage];
}

class PendingStageArrival extends StageInvolvedState {
  const PendingStageArrival(super.stage);

  @override
  List<Object?> get props => [stage];
}

class VisitingStage extends StageInvolvedState {
  const VisitingStage(super.stage);

  @override
  List<Object?> get props => [stage];
}

class PendingStageActivity extends StageInvolvedState {
  // This is required for two consecutive PendingStageActivity states to be regocnized.
  final DateTime momentOfEvent = DateTime.now();

  PendingStageActivity(super.stage);

  @override
  List<Object?> get props => [stage, momentOfEvent];
}

class DisplayingCurrentScore extends RallyState {
  final int latestPoints;
  final int totalPoints;
  final int potentialHighscore;

  String get totalPointsDisplay => '$totalPoints';
  String get potentialHighscoreDisplay => '$potentialHighscore';
  String get correctDisplay => potentialHighscore == 0
      ? '100%'
      : '${(totalPoints / potentialHighscore * 100).round()}%';
  String get lostPointsDisplay => (potentialHighscore - totalPoints).toString();

  const DisplayingCurrentScore(
    this.latestPoints,
    this.totalPoints,
    this.potentialHighscore,
  );

  @override
  List<Object?> get props => [latestPoints, totalPoints, potentialHighscore];
}

class DisplayingFinalScore extends RallyState {}
