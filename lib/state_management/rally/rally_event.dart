part of 'rally_bloc.dart';

abstract class RallyEvent extends Equatable {
  const RallyEvent();

  @override
  List<Object> get props => [];
}

class ResumeRallyRequested extends RallyEvent {}

class RallyJoiningRequested extends RallyEvent {
  final String joiningCode;

  const RallyJoiningRequested({required this.joiningCode});

  @override
  List<Object> get props => [joiningCode];
}

class InvalidJoiningCodeSubmitted extends RallyEvent {
  final String joiningCode;

  const InvalidJoiningCodeSubmitted({required this.joiningCode});

  @override
  List<Object> get props => [joiningCode];
}

class TeamNameConfirmed extends RallyEvent {
  final String teamName;

  const TeamNameConfirmed({required this.teamName});

  @override
  List<Object> get props => [teamName];
}

class TeamMembersConfirmed extends RallyEvent {
  final List<String> teamMembers;

  const TeamMembersConfirmed({required this.teamMembers});

  @override
  List<Object> get props => [teamMembers];
}

class AppSettingsPageRequested extends RallyEvent {}

class BluetoothPermissionGrantRequested extends RallyEvent {}

class BluetoothStatusChanged extends RallyEvent {
  final PermissionStatus bluetoothPermission;
  final ServiceStatus bluetoothService;
  final PermissionStatus bluetoothScanPermission;

  const BluetoothStatusChanged({
    required this.bluetoothPermission,
    required this.bluetoothService,
    required this.bluetoothScanPermission,
  });

  @override
  List<Object> get props => [bluetoothPermission, bluetoothScanPermission];
}

class LocationPermissionGrantRequested extends RallyEvent {}

class StartRangingRequested extends RallyEvent {
  final Stage stage;

  const StartRangingRequested(this.stage);

  @override
  List<Object> get props => [stage];
}

class ReceivedBeaconRangingResult extends RallyEvent {
  final RangingResult rangingResult;

  const ReceivedBeaconRangingResult(this.rangingResult);

  @override
  List<Object> get props => [rangingResult];
}

class ArrivedAtStageLocation extends RallyEvent {
  final Stage stage;
  final String locationMarkerId;

  const ArrivedAtStageLocation({
    required this.stage,
    required this.locationMarkerId,
  });

  @override
  List<Object> get props => [stage];
}

class StageActivityRequested extends RallyEvent {
  final Stage stage;

  const StageActivityRequested(this.stage);

  @override
  List<Object> get props => [stage];
}

class StageInformationRequested extends RallyEvent {
  final Stage stage;

  const StageInformationRequested(this.stage);

  @override
  List<Object> get props => [stage];
}

class AnswerSubmissionRequested extends RallyEvent {
  final String stageActivityId;
  final List<String?> answers;

  const AnswerSubmissionRequested(this.stageActivityId, this.answers);

  @override
  List<Object> get props => [answers];
}

class StageActivityResultReceived extends RallyEvent {
  final int latestPoints;
  final int totalPoints;
  final int potentialHighscore;

  const StageActivityResultReceived({
    required this.latestPoints,
    required this.totalPoints,
    required this.potentialHighscore,
  });

  @override
  List<Object> get props => [latestPoints, totalPoints];
}

class NextStageRequested extends RallyEvent {}

class SkippedStage extends RallyEvent {}

class FinishedRally extends RallyEvent {}
