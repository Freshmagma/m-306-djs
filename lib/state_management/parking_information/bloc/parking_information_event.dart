part of 'parking_information_bloc.dart';

abstract class ParkingInformationEvent extends Equatable {
  const ParkingInformationEvent();

  @override
  List<Object> get props => [];
}

class ParkingInformationRequested extends ParkingInformationEvent {}
