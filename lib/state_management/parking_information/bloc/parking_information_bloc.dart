import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:gibz_mobileapp/data/repositories/parking_information/car_park_repository.dart';
import 'package:gibz_mobileapp/models/parking_information/parking_information.dart';

part 'parking_information_event.dart';
part 'parking_information_state.dart';

class ParkingInformationBloc
    extends Bloc<ParkingInformationEvent, ParkingInformationState> {
  final CarParkRepository carParkRepository;

  ParkingInformationBloc({required this.carParkRepository})
      : super(ParkingInformationInitial()) {
    on<ParkingInformationRequested>(_onParkingInformationRequested);
  }

  FutureOr<void> _onParkingInformationRequested(
    ParkingInformationRequested event,
    Emitter<ParkingInformationState> emit,
  ) async {
    emit(LoadingParkingInformation());

    var carPark = await carParkRepository.getCurrentAvailability();

    if (carPark == null) {
      emit(NoParkingInformationAvailable());
      return null;
    }

    emit(ParkingInformationAvailable(carPark));
  }
}
