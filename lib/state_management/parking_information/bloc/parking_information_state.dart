part of 'parking_information_bloc.dart';

abstract class ParkingInformationState extends Equatable {
  const ParkingInformationState();

  @override
  List<Object> get props => [];
}

class ParkingInformationInitial extends ParkingInformationState {}

class LoadingParkingInformation extends ParkingInformationState {}

class NoParkingInformationAvailable extends ParkingInformationState {}

class ParkingInformationAvailable extends ParkingInformationState {
  final CarPark carPark;

  const ParkingInformationAvailable(this.carPark);

  @override
  List<Object> get props => [carPark];
}
