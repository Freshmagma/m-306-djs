import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/data/api/profile_picture/profile_picture_api.dart';
import 'package:gibz_mobileapp/state_management/progress_indicator_cubit.dart';
import 'package:image/image.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'profile_picture_event.dart';
part 'profile_picture_state.dart';

class ProfilePictureBloc
    extends Bloc<ProfilePictureEvent, ProfilePictureState> {
  final ProgressIndicatorCubit progressIndicatorCubit;
  final ProfilePictureApi profilePictureApi;

  ProfilePictureBloc({
    required this.progressIndicatorCubit,
    required this.profilePictureApi,
  }) : super(ProfilePictureInitial()) {
    on<ProfilePictureProcessLaunched>(_onProfilePictureProcessLaunched);
    on<CameraPermissionGrantRequested>(_onCameraPermissionGrantRequested);
    on<FacePositionInstructionsRead>(_onFacePositionInstructionsRead);
    on<PictureTaken>(_onPictureTaken);
    on<PictureApproved>(_onPictureApproved);
  }

  Future<void> _onProfilePictureProcessLaunched(
    ProfilePictureProcessLaunched event,
    Emitter<ProfilePictureState> emit,
  ) async {
    // TODO: Check if token is valid!
    // final token = await profilePictureApi.getToken(event.token);

    emit(ProfilePictureInitial());
  }

  FutureOr<void> _onCameraPermissionGrantRequested(
    CameraPermissionGrantRequested event,
    Emitter<ProfilePictureState> emit,
  ) async {
    PermissionStatus cameraPermission = await Permission.camera.status;

    if (cameraPermission.isDenied) {
      await Permission.camera.request().then((newPermissionState) {
        if (newPermissionState.isGranted) {
          return _getPendingShuterState();
        }
        return Future.value(PendingCameraPermissionGrant());
      }).then((newState) {
        if (newState != null) {
          emit(newState);
        } else {
          emit(PendingCameraPermissionGrant());
        }
      });
    } else if (cameraPermission.isPermanentlyDenied) {
      openAppSettings();
    } else if (cameraPermission.isGranted) {
      final newState = await _getPendingShuterState();
      if (newState != null) {
        emit(newState);
      } else {
        emit(PendingCameraPermissionGrant());
      }
    }
  }

  FutureOr<void> _onFacePositionInstructionsRead(
    FacePositionInstructionsRead event,
    Emitter<ProfilePictureState> emit,
  ) async {
    final pendingShutterState = await _getPendingShuterState();
    if (pendingShutterState != null) {
      emit(pendingShutterState);
    }
  }

  FutureOr<void> _onPictureTaken(
    PictureTaken event,
    Emitter<ProfilePictureState> emit,
  ) {
    emit(PendingPictureApproval(event.file));
  }

  FutureOr<void> _onPictureApproved(
    PictureApproved event,
    Emitter<ProfilePictureState> emit,
  ) async {
    final path = event.file.path;
    final bytes = await File(path).readAsBytes();
    final image = decodeImage(bytes);

    if (image != null) {
      final png = encodePng(image);

      await profilePictureApi.submitPicture(event.token, png).then((success) {
        if (success) {
          emit(ProfilePictureAccepted());
        } else {
          emit(ProfilePictureRejected());
        }
        _removeSharedPreferenceData();
        progressIndicatorCubit.finish(LoadingAspect.uploadingProfilePicture);
      });
    }
  }

  Future<PendingShutter?> _getPendingShuterState() async {
    return availableCameras().then((cameras) {
      return cameras
          .where((camera) => camera.lensDirection == CameraLensDirection.front)
          .toList()
          .firstOrNull;
    }).then((camera) {
      if (camera != null) {
        return PendingShutter(camera);
      }
      return null;
    });
  }

  Future<void> _removeSharedPreferenceData() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('profile_picture_token');
  }
}
