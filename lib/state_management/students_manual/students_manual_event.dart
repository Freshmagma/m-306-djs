import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

sealed class StudentsManualEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class StudentsManualStarted extends StudentsManualEvent {}

class ArticleFilterUpdated extends StudentsManualEvent {
  final String? filterText;

  ArticleFilterUpdated({this.filterText});

  @override
  List<Object?> get props => [filterText];
}

class ArticleRequested extends StudentsManualEvent {
  final String articleId;
  final String articleTitle;

  ArticleRequested(this.articleId, this.articleTitle);

  @override
  List<Object> get props => [articleId];
}

class DetailViewBackButtonTapped extends StudentsManualEvent {
  final BuildContext context;

  DetailViewBackButtonTapped({required this.context});
}
