import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/data/repositories/students_manual/article_repository.dart';
import 'package:gibz_mobileapp/models/students_manual.dart';
import 'package:gibz_mobileapp/state_management/students_manual/students_manual_event.dart';
import 'package:gibz_mobileapp/state_management/students_manual/students_manual_state.dart';

class StudentsManualBloc
    extends Bloc<StudentsManualEvent, StudentsManualState> {
  final ArticleRepository articleRepository;

  List<Article>? articles;

  StudentsManualBloc({required initialState, required this.articleRepository})
      : super(initialState) {
    on<StudentsManualStarted>((event, emit) async {
      emit(IndexLoadInProgress());
      await _loadArticles()
          .then(_saveArticles)
          .then((articles) =>
              emit(IndexLoadSuccess(_getGroupedArticleList(articles))))
          .catchError((_) {
        emit(IndexLoadFailed());
      });
    });

    on<ArticleFilterUpdated>((event, emit) {
      final filterValue = event.filterText;

      // TODO: Ensure articles list is and not null/empty.
      if (articles == null || articles!.isEmpty) {
        return;
      }

      if (filterValue == null) {
        emit(IndexLoadSuccess(_getGroupedArticleList(articles!)));
      } else {
        final lowerCaseFilterValue = filterValue.toLowerCase();
        final filtered = articles!
            .where((article) =>
                article.title.toLowerCase().contains(lowerCaseFilterValue) ||
                article.content.toLowerCase().contains(lowerCaseFilterValue))
            .toList();
        emit(IndexLoadSuccess(_getGroupedArticleList(filtered)));
      }
    });

    on<ArticleRequested>((event, emit) async {
      emit(ArticleLoadInProgress(
        articleId: event.articleId,
        articleTitle: event.articleTitle,
      ));
      // await articleRepository
      //     .getArticle(event.articleId)
      //     .then((Article article) => emit(ArticleLoadSuccess(
      //           articleId: event.articleId,
      //           articleTitle: event.articleTitle,
      //           article: article,
      //         )))
      //     .catchError((_) => emit(ArticleLoadFailed(
      //           articleId: event.articleId,
      //           articleTitle: event.articleTitle,
      //         )));
    });

    on<DetailViewBackButtonTapped>((event, emit) async {
      final lastState = state;
      if (lastState is ShowDetail) {
        emit(lastState);
      }
      // else if (articles == null || articles!.isEmpty) {
      //   await _loadArticles().then(_saveArticles).then((articles) {
      //     emit(IndexLoadSuccess(_getGroupedArticleList(articles)));
      //   }).catchError((_) {
      //     emit(IndexLoadFailed());
      //   });
      // } else {
      //   emit(IndexLoadSuccess(_getGroupedArticleList(articles!)));
      // }
    });
  }

  Future<List<Article>?> _loadArticles() async {
    return articleRepository.getArticles();
  }

  _saveArticles(List<Article>? articles) {
    this.articles = articles;
    return articles;
  }

  _getGroupedArticleList(List<Article> articles) {
    final Map<String, List<Article>> groupedArticles = {};

    for (var article in articles) {
      final letter = article.title[0].toUpperCase();
      if (groupedArticles[letter] == null) {
        groupedArticles[letter] = <Article>[];
      }

      groupedArticles[letter]!.add(article);
    }

    return groupedArticles;
  }
}
