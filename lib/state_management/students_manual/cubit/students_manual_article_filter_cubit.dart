import 'package:flutter_bloc/flutter_bloc.dart';

class StudentsManualArticleFilterCubit extends Cubit<String> {
  StudentsManualArticleFilterCubit() : super('');

  void updateFilter(String filterText) {
    emit(filterText);
  }
}
