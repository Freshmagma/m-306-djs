import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/navigation_shell.dart';
import 'package:gibz_mobileapp/pages/dashboard.dart';
import 'package:gibz_mobileapp/pages/imprint_page.dart';
import 'package:gibz_mobileapp/pages/meal_information/meal_detail_page.dart';
import 'package:gibz_mobileapp/pages/meal_information/meal_index_page.dart';
import 'package:gibz_mobileapp/pages/parking_information/parking_information_page.dart';
import 'package:gibz_mobileapp/pages/profile_picture/camera_page.dart';
import 'package:gibz_mobileapp/pages/profile_picture/face_position_instructions_page.dart';
import 'package:gibz_mobileapp/pages/profile_picture/picture_approval_page.dart';
import 'package:gibz_mobileapp/pages/profile_picture/profile_picture_accepted_page.dart';
import 'package:gibz_mobileapp/pages/profile_picture/profile_picture_landing_page.dart';
import 'package:gibz_mobileapp/pages/profile_picture/profile_picture_rejected_page.dart';
import 'package:gibz_mobileapp/pages/rally/bluetooth_permission_page.dart';
import 'package:gibz_mobileapp/pages/rally/final_score_page.dart';
import 'package:gibz_mobileapp/pages/rally/rally_start_page.dart';
import 'package:gibz_mobileapp/pages/rally/score_page.dart';
import 'package:gibz_mobileapp/pages/rally/stage_activity_page.dart';
import 'package:gibz_mobileapp/pages/rally/location_permission_page.dart';
import 'package:gibz_mobileapp/pages/rally/pending_stage_arrival_page.dart';
import 'package:gibz_mobileapp/pages/rally/rally_code_page.dart';
import 'package:gibz_mobileapp/pages/rally/stage_page.dart';
import 'package:gibz_mobileapp/pages/rally/team_members_page.dart';
import 'package:gibz_mobileapp/pages/rally/team_name_page.dart';
import 'package:gibz_mobileapp/pages/students_manual/students_manual_detail_page.dart';
import 'package:gibz_mobileapp/pages/students_manual/students_manual_index_page.dart';
import 'package:go_router/go_router.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'state_management/rally/rally_bloc.dart';

final GlobalKey<NavigatorState> _rootNavigatorKey =
    GlobalKey<NavigatorState>(debugLabel: 'root');
final GlobalKey<NavigatorState> _shellNavigatorKey =
    GlobalKey<NavigatorState>(debugLabel: 'shell');

// GoRouter configuration
final router = GoRouter(
  navigatorKey: _rootNavigatorKey,
  initialLocation: '/',
  debugLogDiagnostics: true,
  routes: <RouteBase>[
    ShellRoute(
      navigatorKey: _shellNavigatorKey,
      builder: (context, state, child) => NavigationShell(child: child),
      routes: <RouteBase>[
        GoRoute(
          path: '/',
          builder: (context, state) => const Dashboard(),
          routes: [
            GoRoute(
              path: 'gibzrally',
              builder: (context, state) =>
                  const Placeholder(message: 'GIBZ Rallye'),
              redirect: (context, state) {
                if (state.fullPath == '/gibzrally/start') {
                  return null;
                }
                switch (context.read<RallyBloc>().state.runtimeType) {
                  case RallyInitial:
                    return '/gibzrally/code';
                  case PendingTeamNameCompletion:
                    return '/gibzrally/teamname';
                  case PendingTeamMembersCompletion:
                    return '/gibzrally/members';
                  case PendingBluetoothPermissionGrant:
                    return '/gibzrally/bluetooth';
                  case PendingLocationPermissionGrant:
                    return '/gibzrally/location';
                  case PendingStageArrival:
                    return '/gibzrally/pending-arrival';
                  case VisitingStage:
                    return '/gibzrally/stage';
                  case PendingStageActivity:
                    return '/gibzrally/stage/challenge';
                  case DisplayingCurrentScore:
                    return '/gibzrally/score';
                  case DisplayingFinalScore:
                    return '/gibzrally/final-score';
                  default:
                    return null;
                }
              },
            ),
            GoRoute(
              path: 'gibzrally/start',
              builder: (context, state) => const RallyStartPage(),
            ),
            GoRoute(
              path: 'gibzrally/code',
              builder: (context, state) => const RallyCodePage(),
            ),
            GoRoute(
              path: 'gibzrally/teamname',
              builder: (context, state) => const TeamNamePage(),
            ),
            GoRoute(
              path: 'gibzrally/members',
              builder: (context, state) => const TeamMembersPage(),
            ),
            GoRoute(
              path: 'gibzrally/bluetooth',
              builder: (context, state) => const BluetoothPermissionPage(),
            ),
            GoRoute(
              path: 'gibzrally/location',
              builder: (context, state) => const LocationPermissionPage(),
            ),
            GoRoute(
              path: 'gibzrally/pending-arrival',
              builder: (context, state) => const PendingStageArrivalPage(),
            ),
            GoRoute(
              path: 'gibzrally/stage',
              builder: (context, state) => const StagePage(),
              routes: [
                GoRoute(
                  path: 'challenge',
                  builder: (context, state) => const StageActivityPage(),
                ),
              ],
            ),
            GoRoute(
              path: 'gibzrally/score',
              builder: (context, state) => const ScorePage(),
            ),
            GoRoute(
              path: 'gibzrally/final-score',
              builder: (context, state) => const FinalScorePage(),
            ),
            GoRoute(
              path: 'meal-information',
              builder: (context, state) => const MealIndexPage(),
              routes: [
                GoRoute(
                  path: ':mealId',
                  builder: (context, state) => MealDetailPage(
                    mealId: state.pathParameters['mealId']!,
                  ),
                )
              ],
            ),
            GoRoute(
              path: 'parking-information',
              builder: (context, state) => const ParkingInformationPage(),
            ),
            GoRoute(
              path: 'profile-picture/:token',
              builder: (context, state) => ProfilePictureLandingPage(
                token: state.pathParameters['token']!,
              ),
              redirect: (context, state) async {
                // locally persist token
                final token = state.pathParameters['token']!;
                final prefs = await SharedPreferences.getInstance();
                await prefs.setString('profile_picture_token', token);

                if (await Permission.camera.isGranted) {
                  return '/profile-picture/$token/face-instructions';
                }

                return null;
              },
            ),
            GoRoute(
                path: 'profile-picture/:token/face-instructions',
                builder: (context, state) => FacePositionInstructionsPage(
                      token: state.pathParameters['token']!,
                    ),
                routes: [
                  GoRoute(
                    path: 'camera',
                    builder: (context, state) => CameraPage(
                      token: state.pathParameters['token']!,
                    ),
                    routes: [
                      GoRoute(
                        path: 'approval',
                        builder: (context, state) => PictureApprovalPage(
                          token: state.pathParameters['token']!,
                          file: state.extra as XFile,
                        ),
                      ),
                    ],
                  ),
                ]),
            GoRoute(
              path: 'profile-picture/:token/accepted',
              parentNavigatorKey: _rootNavigatorKey,
              builder: (context, state) => ProfilePictureAcceptedPage(
                token: state.pathParameters['token']!,
              ),
            ),
            GoRoute(
              path: 'profile-picture/:token/rejected',
              parentNavigatorKey: _rootNavigatorKey,
              builder: (context, state) => ProfilePictureRejectedPage(
                token: state.pathParameters['token']!,
              ),
            ),
            GoRoute(
              path: 'students-manual',
              builder: (context, state) => const StudentsManualIndexPage(),
              routes: [
                GoRoute(
                  path: ':articleId',
                  builder: (context, state) => StudentsManualDetailPage(
                    articleId: state.pathParameters['articleId']!,
                  ),
                )
              ],
            ),
            GoRoute(
              path: 'imprint',
              builder: (context, state) => const ImprintPage(),
            )
          ],
        ),
      ],
    ),
  ],
);

// TODO: Remove placeholder
class Placeholder extends StatelessWidget {
  const Placeholder({super.key, this.message});

  final String? message;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(message ?? "Placeholder"),
      ),
    );
  }
}
