import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/models/initialize_mappers.dart';
import 'package:gibz_mobileapp/routing.dart';
import 'package:gibz_mobileapp/widgets/gibz_repository_provider.dart';
import 'package:google_fonts/google_fonts.dart';

import 'state_management/progress_indicator_cubit.dart';

void main() {
  initializeMappers();
  runApp(
    BlocProvider(
      create: (_) => ProgressIndicatorCubit(),
      child: const GibzRepositoryProvider(child: GibzApp()),
    ),
  );
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
}

class GibzApp extends StatelessWidget {
  const GibzApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'GIBZ App',
      routerConfig: router,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          seedColor: const Color.fromRGBO(0, 119, 192, 1),
          primary: const Color.fromRGBO(0, 119, 192, 1), // #0077C0
          secondary: const Color.fromRGBO(207, 212, 222, 1), // #CFD4DE
          tertiary: const Color.fromRGBO(47, 63, 92, 1),
          background: const Color.fromRGBO(238, 240, 244, 1),
        ),
        useMaterial3: true,
        textTheme:
            GoogleFonts.interTextTheme(Theme.of(context).textTheme).copyWith(
          headlineLarge: const TextStyle(
            fontSize: 28,
            fontWeight: FontWeight.w600, //ok
          ),
          headlineMedium: const TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.normal, //ok
            color: Color.fromRGBO(47, 63, 92, 1),
          ),
          headlineSmall: const TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500, // ok
            color: Color.fromRGBO(47, 63, 92, 1),
          ),
          titleLarge: const TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w600, // ok
            color: Color.fromRGBO(47, 63, 92, 1),
          ),
          titleMedium: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w600,
          ),
          titleSmall: const TextStyle(
            fontSize: 13,
            fontWeight: FontWeight.w600, // ok
          ),
          bodyMedium: const TextStyle(
            color: Color.fromRGBO(47, 63, 92, 1),
            fontWeight: FontWeight.w400, // ok
          ),
          bodySmall: const TextStyle(
            color: Color.fromRGBO(47, 63, 92, 1),
          ),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            foregroundColor:
                MaterialStateProperty.resolveWith<Color?>((states) {
              if (states.contains(MaterialState.disabled)) {
                return null;
              }
              return Colors.white;
            }),
            backgroundColor:
                MaterialStateProperty.resolveWith<Color?>((states) {
              if (states.contains(MaterialState.disabled)) {
                return null;
              }
              return const Color.fromRGBO(0, 119, 192, 1);
            }),
            textStyle: MaterialStateProperty.all<TextStyle>(
              const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
        textButtonTheme: TextButtonThemeData(
          style: ButtonStyle(
            overlayColor: MaterialStateColor.resolveWith(
              (states) => Colors.transparent,
            ),
            foregroundColor: MaterialStateColor.resolveWith((states) {
              if (states.contains(MaterialState.pressed) ||
                  states.contains(MaterialState.selected)) {
                return Theme.of(context).colorScheme.primary.withAlpha(100);
              }
              return Theme.of(context).colorScheme.primary;
            }),
            textStyle: MaterialStateProperty.all(
              const TextStyle(fontWeight: FontWeight.normal),
            ),
          ),
        ),
        inputDecorationTheme: InputDecorationTheme(
          filled: true,
          fillColor: Colors.white,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(300),
            borderSide: BorderSide.none,
          ),
          isDense: true,
          hintStyle: const TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.normal,
            color: Color.fromRGBO(47, 63, 92, 1),
          ),
          contentPadding: const EdgeInsets.fromLTRB(20, 10, 10, 10),
          suffixIconColor: const Color.fromRGBO(131, 143, 166, 1),
        ),
      ),
    );
  }
}
